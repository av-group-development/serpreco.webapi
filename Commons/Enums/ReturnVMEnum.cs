﻿namespace Commons.Enums
{
    public enum ReturnVMEnum
    {
        SavedOk = 1,
        SavedKo = 2,
        LoggedKo = 3,
        EmailSentOk = 4,
        EmailSentKo = 5,
        QuestionaireSentOk = 6,
        BudgetSentOk = 7,
        RequestSentOk = 8,
        MakeCallOk = 9
    }
}