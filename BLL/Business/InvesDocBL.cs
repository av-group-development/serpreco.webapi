﻿using BLL.IBusiness;
using DAL.Utils;
using DAL.ViewModel;
using Serpreco.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Business
{
    public class InvesDocBL : IInvesDocBL
    {
        public List<StudioProposalInvesDocumentModel> GetStudioDocuments(int studyId, int proposalId, string sPerson)
        {
            try
            {
                InvesDocService docService = new InvesDocService();

                return docService.getDocumentsStudy(studyId, proposalId, sPerson);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UploadStudioSingleFile(StudioInvesDocumentModelDAL document)
        {
            try
            {
                InvesDocService docService = new InvesDocService();

                return docService.UploadStudioSingleFile(document);
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
    }
}
