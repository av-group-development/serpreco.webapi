﻿using DAL.Entity;
using DAL.Repository;
using BLL.IBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Business
{
    
    public  class MasterTablesBL  : IMasterTablesBL
    {
        //  private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region ScheduleStates

        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ScheduleStates Get(Expression<Func<ScheduleStates, bool>> filter)
        {
            try
            {
                ScheduleStatesRepository  sr = new ScheduleStatesRepository();
                return sr.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                // log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<ScheduleStates> GetList(Expression<Func<ScheduleStates, bool>> filter)
        {
            try
            {
                ScheduleStatesRepository sr = new ScheduleStatesRepository();
                return sr.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion

        #region AnnotationsStates
        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public AnnotationsStates Get(Expression<Func<AnnotationsStates, bool>> filter)
        {
            try
            {
                AnnotationsStatesRepository sr = new AnnotationsStatesRepository();
                return sr.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                // log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<AnnotationsStates> GetList(Expression<Func<AnnotationsStates, bool>> filter)
        {
            try
            {
                AnnotationsStatesRepository sr = new AnnotationsStatesRepository();
                return sr.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //     log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion

        #region AnnotationsTypes
        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public AnnotationsTypes Get(Expression<Func<AnnotationsTypes, bool>> filter)
        {
            try
            {
                AnnotationsTypesRepository ar = new AnnotationsTypesRepository();
                return ar.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //   log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<AnnotationsTypes> GetList(Expression<Func<AnnotationsTypes, bool>> filter)
        {
            try
            {
                AnnotationsTypesRepository ar = new AnnotationsTypesRepository();
                return ar.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                // log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion

        #region AnnotationsSubtypes
        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public AnnotationsSubtypes Get(Expression<Func<AnnotationsSubtypes, bool>> filter)
        {
            try
            {
                AnnotationsSubtypesRepository ar = new AnnotationsSubtypesRepository();
                return ar.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<AnnotationsSubtypes> GetList(Expression<Func<AnnotationsSubtypes, bool>> filter)
        {
            try
            {
                AnnotationsSubtypesRepository ar = new AnnotationsSubtypesRepository();
                return ar.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion

        #region ScheduleTypes
        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ScheduleTypes Get(Expression<Func<ScheduleTypes, bool>> filter)
        {
            try
            {
                ScheduleTypesRepository sr = new ScheduleTypesRepository();
                return sr.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<ScheduleTypes> GetList(Expression<Func<ScheduleTypes, bool>> filter)
        {
            try
            {
                ScheduleTypesRepository sr = new ScheduleTypesRepository();
                return sr.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion

        #region ScheduleEntities
        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ScheduleEntities Get(Expression<Func<ScheduleEntities, bool>> filter)
        {
            try
            {
                ScheduleEntitiesRepository sr = new ScheduleEntitiesRepository();
                return sr.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<ScheduleEntities> GetList(Expression<Func<ScheduleEntities, bool>> filter)
        {
            try
            {
                ScheduleEntitiesRepository sr = new ScheduleEntitiesRepository();
                return sr.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //    log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion

        #region SchedulePriorities
        /// <summary>
        /// Get entity result by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public SchedulePriorities Get(Expression<Func<SchedulePriorities, bool>> filter)
        {
            try
            {
                SchedulePrioritiesRepository sr = new SchedulePrioritiesRepository();
                return sr.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<SchedulePriorities> GetList(Expression<Func<SchedulePriorities, bool>> filter)
        {
            try
            {
                SchedulePrioritiesRepository sr = new SchedulePrioritiesRepository();
                return sr.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        #endregion
    }
}
