﻿using BLL.IBusiness;
using DAL.Entity;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BLL.Business
{
    public class NotificationBL : INotificationBL
    {
       // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Notification Add(Notification notification)
        {
            NotificationRepository nr = new NotificationRepository();

            nr.CurrentContext.Notification.AddObject(notification);
            nr.CurrentContext.SaveChanges();

            return notification;
        }

        public Notification Get(Expression<Func<Notification, Boolean>> filter)
        {
            NotificationRepository nr = new NotificationRepository();
            return nr.GetFilters(filter).FirstOrDefault();
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Notification> GetList(Expression<Func<Notification, Boolean>> filter)
        {
            NotificationRepository nr = new NotificationRepository();
            return nr.GetFilters(filter).ToList();
        }


        public bool Update(Notification notification)
        {
            NotificationRepository nr = new NotificationRepository();
            Expression<Func<Notification, Boolean>> filter = (r => r.Id == notification.Id);

            Notification notif = nr.GetFilters(filter).FirstOrDefault();
            notif.Description = notification.Description;
            notif.IdUser = notification.IdUser;
            notif.IsRead = notification.IsRead;
            notif.DateNotification = notification.DateNotification;

            nr.CurrentContext.Notification.Attach(notif);
            nr.CurrentContext.ObjectStateManager.ChangeObjectState(notif, System.Data.Entity.EntityState.Modified);
            nr.CurrentContext.SaveChanges();
            return true;
        }

        public bool Delete(Notification notification)
        {
            NotificationRepository nr = new NotificationRepository();
            Expression<Func<Notification, Boolean>> filter = (r => r.Id == notification.Id);
            Notification notif = nr.GetFilters(filter).FirstOrDefault();

            nr.CurrentContext.Notification.DeleteObject(notif);
            nr.CurrentContext.SaveChanges();
            return true;
        }


        public bool UpdateIsRead(Notification notification)
        {
            NotificationRepository nr = new NotificationRepository();
            Expression<Func<Notification, Boolean>> filter = (r => r.Id == notification.Id);

            Notification notif = nr.GetFilters(filter).FirstOrDefault();
            notif.IsRead = true;

            return Update(notif);
        }



        public bool UpdateDescription(Notification notification, string newDescription)
        {
            NotificationRepository nr = new NotificationRepository();
            Expression<Func<Notification, Boolean>> filter = (r => r.Id == notification.Id);

            Notification notif = nr.GetFilters(filter).FirstOrDefault();
            notif.Description = newDescription;

            return Update(notif);
        }


        public bool UpdateDateNotification(Notification notification, DateTime dateNotification)
        {
            NotificationRepository nr = new NotificationRepository();
            Expression<Func<Notification, Boolean>> filter = (r => r.Id == notification.Id);

            Notification notif = nr.GetFilters(filter).FirstOrDefault();
            IUtilBL ubl = new UtilBL();
            notif.DateNotification = dateNotification.AddDays(ubl.GetPrevLaboralDayBefore48hByDate(dateNotification));

            return Update(notif);
        }

        public bool CreateNotificationBySchedule(Schedule schedule)
        {
            Notification notif = new Notification();
            notif.Description = schedule.Note;
            notif.IdUser = schedule.IdUserAssigned;
            notif.IsRead = false;
            notif.IdSchedule = schedule.Id;
            DateTime datePlan = (DateTime)schedule.DatePlanned;

            IUtilBL ubl = new UtilBL();
            notif.DateNotification = datePlan.AddDays(ubl.GetPrevLaboralDayBefore48hByDate(datePlan));
            Add(notif);
            return true;
        }
    }
}
