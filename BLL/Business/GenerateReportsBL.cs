﻿using DevExpress.XtraReports.UI;
using BLL.IBusiness;
using System;
using System.Globalization;
using System.IO;
using DAL.ViewModel;


namespace BLL.Business
{
    public class GenerateReportsBL : IGenerateReports
    {
        public DocumentModel GenerateDocumentSolicitud(int proposalId,string idioma)
        {
            DocumentModel document = new DocumentModel();

            document.Nombre = "solicitud_proposal.pdf";
            document.Extension = "pdf";
            byte[] respuesta = this.generateReport(proposalId, idioma);

            if (respuesta != null)
            {
                document.DocumentBase64 = Convert.ToBase64String(respuesta);
            }
            
            return document;
        }

        private byte[] generateReport(int proposalId,string idioma)
        {
            byte[] result = null;

            XtraReport report = null;
            

            switch (idioma)
            {
                case "es":
                    report =  null; //TODO: new  Solicitud_es();
                    break;
                case "ca":
                    report = null; //TODO: new Solicitud_ca();
                    break;
                default:
                    break;
            }
                        
            if (report != null)
            {
                if (report.Parameters["SitioFecha"] != null)
                {
                    report.Parameters["SitioFecha"].Value = $"Bacelona, el {DateTime.Now.Day} de {CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month)} de {DateTime.Now.Year}";                    
                }

                if (report.Parameters["ProposalId"] != null)
                {
                    report.Parameters["ProposalId"].Value = proposalId;
                }

                MemoryStream PDFReport = new MemoryStream();
                report.ExportToPdf(PDFReport);

                result = PDFReport.GetBuffer();
            }

            return result;

        }
    }
}
