﻿using BLL.IBusiness;
using DAL.Entity;
using DAL.Enums;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BLL.Business
{
    public class AnnotationsBL : IAnnotationsBL
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Annotations Add(Annotations annotations)
        {
            AnnotationsRepository ar = new AnnotationsRepository();
             
            ar.CurrentContext.Annotations.AddObject(annotations);
            ar.CurrentContext.SaveChanges();

            return annotations;
        }

        public Annotations Get(Expression<Func<Annotations, Boolean>> filter)
        {
            AnnotationsRepository sr = new AnnotationsRepository();
            return sr.GetFilters(filter).FirstOrDefault();
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Annotations> GetList(Expression<Func<Annotations, Boolean>> filter)
        {
            AnnotationsRepository sr = new AnnotationsRepository();
            return sr.GetFilters(filter).ToList();
        }

        public bool Delete(Annotations annotations)
        {
            AnnotationsRepository ar = new AnnotationsRepository();
            Expression<Func<Annotations, Boolean>> filter = (r => r.Id == annotations.Id);
            Annotations annotation = ar.GetFilters(filter).FirstOrDefault();

            ar.CurrentContext.Annotations.DeleteObject(annotation);
            ar.CurrentContext.SaveChanges();
            return true;
        }

        public bool Update(Annotations annotations)
        {
            AnnotationsRepository ar = new AnnotationsRepository();
            Expression<Func<Annotations, Boolean>> filter = (r => r.Id == annotations.Id);

            Annotations annotation = ar.GetFilters(filter).FirstOrDefault();
            annotation.IdScheduleEntities = annotations.IdScheduleEntities;
            annotation.CodEntity = annotations.CodEntity;
            annotation.IdAnnotationTypes = annotations.IdAnnotationTypes;
            annotation.IdAnnotationSubtypes = annotations.IdAnnotationSubtypes;
            annotation.IdAnnotationsStates = annotations.IdAnnotationsStates;
            annotation.IdUserCreated = annotations.IdUserCreated;
            annotation.Note = annotations.Note;
            annotation.DateEffect = annotations.DateEffect;
            annotation.DateCreation = annotations.DateCreation;
            annotation.UltModProcess = annotations.UltModProcess;
            annotation.UltModUser = annotations.UltModUser;
            annotation.UltModTimeStamp = annotations.UltModTimeStamp;
            annotation.UltInsUser = annotations.UltInsUser;
            annotation.UltInsTimeStamp = annotations.UltInsTimeStamp;

            ar.CurrentContext.Annotations.Attach(annotation);
            ar.CurrentContext.ObjectStateManager.ChangeObjectState(annotation, System.Data.Entity.EntityState.Modified);
            ar.CurrentContext.SaveChanges();
            return true;
        }


        public bool UpdateByModal(Annotations annotations)
        {
            AnnotationsRepository ar = new AnnotationsRepository();
            Expression<Func<Annotations, Boolean>> filter = (r => r.Id == annotations.Id);

            Annotations annotation = ar.GetFilters(filter).FirstOrDefault();
            annotation.IdAnnotationTypes = annotations.IdAnnotationTypes;
            annotation.IdAnnotationSubtypes = annotations.IdAnnotationSubtypes;
            annotation.IdAnnotationsStates = annotations.IdAnnotationsStates;
            annotation.Note = annotations.Note;
            annotation.DateEffect = annotations.DateEffect;
     
            ar.CurrentContext.Annotations.Attach(annotation);
            ar.CurrentContext.ObjectStateManager.ChangeObjectState(annotation, System.Data.Entity.EntityState.Modified);
            ar.CurrentContext.SaveChanges();
            return true;
        }

        public List<Annotations> GetGrid(Expression<Func<Annotations, Boolean>> filter)
        {
            AnnotationsRepository ar = new AnnotationsRepository();
            ar.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
            ar.CurrentContext.ContextOptions.LazyLoadingEnabled = true;

            List<Annotations> annotation = ar.GetFilters(filter).ToList(); 
            List<Annotations> AnnotationsList = new List<Annotations>();
            foreach (var a in annotation)
            {
                Annotations aa = new Annotations();
                aa.Id = a.Id;
                aa.IdScheduleEntities = a.IdScheduleEntities;
                aa.CodEntity = a.CodEntity;
                aa.IdAnnotationTypes = a.IdAnnotationTypes;
                aa.IdAnnotationSubtypes = a.IdAnnotationSubtypes;
                aa.IdAnnotationsStates = a.IdAnnotationsStates;
                aa.IdUserCreated = a.IdUserCreated;
                aa.Note = a.Note;
                aa.DateEffect = a.DateEffect;
                aa.DateCreation = a.DateCreation;
                aa.UltModProcess = a.UltModProcess;
                aa.UltModUser = a.UltModUser;
                aa.UltModTimeStamp = a.UltModTimeStamp;
                aa.UltInsUser = a.UltInsUser;
                aa.UltInsTimeStamp = a.UltInsTimeStamp;
/*
                aa.IdAnnotationTypesDesc = a.AnnotationsTypes.Description;
                aa.IdAnnotationSubtypesDesc = a.AnnotationsSubtypes.Description;
                aa.IdAnnotationsStatesDesc = a.AnnotationsStates.Description;
                aa.IdUserCreatedDesc = a.AspNetUsers.UserName;
                aa.IdAnnotationTypesTranslate = a.AnnotationsTypes.Translation;
                aa.IdAnnotationSubtypesTranslate = a.AnnotationsSubtypes.Translation;
                aa.IdAnnotationsStatesTranslate = a.AnnotationsStates.Translation;
                   */
                AnnotationsList.Add(aa);
            }
            return AnnotationsList;
        }

        public List<Annotations> GetListCompleted(Expression<Func<Annotations, Boolean>> filter)
        {
            AnnotationsRepository ar = new AnnotationsRepository();
            ar.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
            ar.CurrentContext.ContextOptions.LazyLoadingEnabled = true;

            List<Annotations> annotation = ar.GetFilters(filter).ToList();
            List<Annotations> AnnotationsList = new List<Annotations>();
            foreach (var a in annotation)
            {
                Annotations aa = new Annotations();
                aa.Id = a.Id;
                aa.IdScheduleEntities = a.IdScheduleEntities;
                aa.CodEntity = a.CodEntity;
                aa.IdAnnotationTypes = a.IdAnnotationTypes;
                aa.IdAnnotationSubtypes = a.IdAnnotationSubtypes;
                aa.IdAnnotationsStates = a.IdAnnotationsStates;
                aa.IdUserCreated = a.IdUserCreated;
                aa.Note = a.Note;
                aa.DateEffect = a.DateEffect;
                aa.DateCreation = a.DateCreation;
                aa.UltModProcess = a.UltModProcess;
                aa.UltModUser = a.UltModUser;
                aa.UltModTimeStamp = a.UltModTimeStamp;
                aa.UltInsUser = a.UltInsUser;
                aa.UltInsTimeStamp = a.UltInsTimeStamp;

                //TODO: PARTIAL
                /*
                aa.IdAnnotationTypesDesc = a.AnnotationsTypes.Description;
                aa.IdAnnotationSubtypesDesc = a.AnnotationsSubtypes.Description;
                aa.IdAnnotationsStatesDesc = a.AnnotationsStates.Description;
                aa.IdUserCreatedDesc = a.AspNetUsers.UserName;
                aa.IdAnnotationTypesTranslate = a.AnnotationsTypes.Translation;
                aa.IdAnnotationSubtypesTranslate = a.AnnotationsSubtypes.Translation;
                aa.IdAnnotationsStatesTranslate = a.AnnotationsStates.Translation;
                */
                   
                StudioRepository str = new StudioRepository();
                str.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
                str.CurrentContext.ContextOptions.LazyLoadingEnabled = true;
                Expression<Func<Studio, Boolean>> filter2 = (r => r.Id == a.CodEntity);
                Studio st = str.GetFilters(filter2).FirstOrDefault();


                if (st != null)
                {
                   // TODO: PÀRTIAL
                  //  aa.IdPersonDesc = st.Person != null ? st.Person.Name + " " + st.Person.Surname1 : string.Empty;

                    //sera  st.Person.FullName
                }
                else
                {// TODO: PÀRTIAL
                 // aa.IdPersonDesc = string.Empty;
                }
                AnnotationsList.Add(aa);
            }
            return AnnotationsList;
        }

        public bool CreateAnnotationBySchedule(Schedule schedule)
        {
            Annotations annotations = new Annotations();
            annotations.IdScheduleEntities = schedule.IdScheduleEntities; //ScheduleEntities.Id;
            annotations.CodEntity = schedule.CodEntity;
            annotations.DateEffect = DateTime.Now;
            annotations.DateCreation = DateTime.Now;
            // annotations.Note = schedule.Note;  //de moment no la passem
            annotations.IdUserCreated = schedule.UltModUser;

            if (schedule.IdScheduleStates == (int)ScheduleStatesEnum.hecho)
            {
                annotations.IdAnnotationsStates = (int)AnnotationsStatesEnum.hecho;
                annotations.IdAnnotationTypes = (int)AnnotationsTypesEnum.cambioEstado;
                annotations.IdAnnotationSubtypes = (int)AnnotationsSubtypesEnum.cerrado;
                //si la tasca passe a fet, creem l'anotació de la tasca, en aquest cas copio la nota
                annotations.Note = schedule.Note;
            }
            else if (schedule.IdScheduleStates == (int)ScheduleStatesEnum.cancelado)
            {
                annotations.IdAnnotationsStates = (int)AnnotationsStatesEnum.cancelado;
                annotations.IdAnnotationTypes = (int)AnnotationsTypesEnum.cambioEstado;
                annotations.IdAnnotationSubtypes = (int)AnnotationsSubtypesEnum.anulado;
            }
            else if (schedule.IdScheduleStates == (int)ScheduleStatesEnum.pendiente)
            {
                annotations.IdAnnotationsStates = (int)AnnotationsStatesEnum.pendiente;
                annotations.IdAnnotationTypes = (int)AnnotationsTypesEnum.cambioEstado;
                annotations.IdAnnotationSubtypes = (int)AnnotationsSubtypesEnum.abierto;
            }

            Add(annotations);
            return true;
        }
    }
}
