﻿using DAL.Entity;
using BLL.IBusiness;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Http;
using System.Net.Http;

namespace BLL.Business
{
    public class AssesorsBL : IAssesorsBL
    {
       // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Assesors> GetList()
        {
            try
            {
                Expression<Func<Assesors, bool>> filterAll = e => e.Id > 0;
                AssesorsRepository repo = new AssesorsRepository();

                return repo.GetFilters(filterAll).OrderBy(p => p.Nombre).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
            //    log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
    }
}
