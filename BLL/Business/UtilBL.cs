﻿using BLL.IBusiness;
using System;
using System.Data.SqlClient;
using System.Globalization;

namespace BLL.Business
{
    public class UtilBL : IUtilBL
    {/*
        public string AccessAnotherConnectionString(string connString, string selectStatement)
        {
            string result = string.Empty;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand comm = new SqlCommand(selectStatement, conn))
                {
                    try
                    {
                        conn.Open();
                        using (SqlDataReader dr = comm.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    result = (dr["xxxx"].ToString()); //por poner algo de ejemplo
                                   
                                }
                            }
                            else
                            { }
                        }
                    }
                    catch (Exception e) { Console.WriteLine("Error: " + e.Message); }
                    if (conn.State == System.Data.ConnectionState.Open) conn.Close();
                }
            }
            return result;
        }*/

        /// <summary>
        /// return the next day for assing task before 48h
        /// </summary>
        /// <returns>int to sum</returns>
        public int GetPrevLaboralDayBefore48hByDate(DateTime Date)
        {
            return DateTime.ParseExact(Date.AddDays(-2).ToString("yyyy-MM-dd"), "yyyy-MM-dd", CultureInfo.InvariantCulture).DayOfWeek == DayOfWeek.Saturday ? -3 :
                     DateTime.ParseExact(Date.AddDays(-2).ToString("yyyy-MM-dd"), "yyyy-MM-dd", CultureInfo.InvariantCulture).DayOfWeek == DayOfWeek.Sunday ? -4 : -2;
        }
    }
}
