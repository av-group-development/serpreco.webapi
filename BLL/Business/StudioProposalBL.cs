﻿using DAL.Entity;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Business
{
    public class StudioProposalBL
    {
        // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// GetStudioProposalById
        /// </summary>
        /// <returns>List<VStudio></returns>
        public StudioProposal GetStudioProposalById(int proposalId)
        {
            StudioProposalRepository spRep = new StudioProposalRepository();

            try
            {
                Expression<Func<StudioProposal, bool>> filter = e => e.Id == proposalId;
                return spRep.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// GetStudioProposalById
        /// </summary>
        /// <returns>List<VStudio></returns>
        public List<StudioProposal> GetStudioProposalByStudioId(int studioId)
        {
            StudioProposalRepository spRep = new StudioProposalRepository();

            try
            {
                Expression<Func<StudioProposal, bool>> filter = e => e.StudioId == studioId && e.DateDelete == null;
                return spRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Add new entity
        /// </summary>
        /// <param name="studio">object of entity</param>
        public StudioProposal Add(StudioProposal proposal)
        {
            StudioProposalRepository spRep = new StudioProposalRepository();

            try
            {
                if (proposal.PeriodicityId == 0) proposal.PeriodicityId = null;
                proposal.DateCreate = DateTime.UtcNow;
                proposal.CreatedBy = 1;
                proposal.UpdatedBy = 1;
                spRep.CurrentContext.StudioProposal.AddObject(proposal);
                spRep.CurrentContext.SaveChanges();
                return proposal;
            }
            catch (HttpResponseException ex)
            {
                //    log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <returns></returns>
        public StudioProposal Update(StudioProposal proposal)
        {
            StudioProposalRepository spRep = new StudioProposalRepository();

            try
            {
                proposal.DateUpdate = DateTime.UtcNow;
                proposal.UpdatedBy = 1;
                spRep.CurrentContext.StudioProposal.Attach(proposal);
                spRep.CurrentContext.ObjectStateManager.ChangeObjectState(proposal, System.Data.Entity.EntityState.Modified);
                spRep.CurrentContext.SaveChanges();
                return proposal;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public StudioProposal Delete(StudioProposal proposal)
        {
            StudioProposalRepository spRep = new StudioProposalRepository();

            try
            {
                proposal.DateDelete = DateTime.UtcNow;
                proposal.DeletedBy = 1;
                spRep.CurrentContext.StudioProposal.Attach(proposal);
                spRep.CurrentContext.ObjectStateManager.ChangeObjectState(proposal, System.Data.Entity.EntityState.Modified);
                spRep.CurrentContext.SaveChanges();
                return proposal;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
    }
}
