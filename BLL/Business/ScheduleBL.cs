﻿using BLL.IBusiness;
using DAL.Entity;
using DAL.Enums;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BLL.Business
{
    public class ScheduleBL : IScheduleBL
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Schedule Add(Schedule schedule)
        {  
            ScheduleRepository sr = new ScheduleRepository();
            Schedule newSchedule = new Schedule();

            newSchedule.IdScheduleEntities = schedule.IdScheduleEntities;
            newSchedule.CodEntity = schedule.CodEntity;
            newSchedule.IdScheduleStates = schedule.IdScheduleStates;
            newSchedule.IdScheduleTypes = schedule.IdScheduleTypes;
            newSchedule.IdUserCreated = schedule.IdUserCreated;
            newSchedule.IdUserAssigned = schedule.IdUserAssigned == string.Empty ? schedule.IdUserCreated : schedule.IdUserAssigned;
            newSchedule.IdSchedulePriorities = schedule.IdSchedulePriorities;
            newSchedule.Note = schedule.Note;
            newSchedule.avis = schedule.avis;
            newSchedule.DateCreation = schedule.DateCreation;
            newSchedule.DatePlanned = schedule.DatePlanned == null ? DateTime.Now : (DateTime)schedule.DatePlanned;
            newSchedule.UltModProcess = schedule.UltModProcess;
            newSchedule.UltModUser = schedule.UltModUser;
            newSchedule.UltModTimeStamp = schedule.UltModTimeStamp;
            newSchedule.UltInsUser = schedule.UltInsUser;
            newSchedule.UltInsTimeStamp = schedule.UltInsTimeStamp;

            sr.CurrentContext.Schedule.AddObject(newSchedule);
            sr.CurrentContext.SaveChanges();

            if ((bool)newSchedule.avis)
            {
                NotificationBL nbl = new NotificationBL();
                nbl.CreateNotificationBySchedule(newSchedule);
            }

            return newSchedule;
        }

        public Schedule Get(Expression<Func<Schedule, Boolean>> filter)
        {
            ScheduleRepository sr = new ScheduleRepository();
            return sr.GetFilters(filter).FirstOrDefault();
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Schedule> GetList(Expression<Func<Schedule, Boolean>> filter)
        {
            ScheduleRepository sr = new ScheduleRepository();
            return sr.GetFilters(filter).ToList();
        }

        public bool Delete(Schedule schedule)
        {  
            ScheduleRepository sr = new ScheduleRepository();
            Expression<Func<Schedule, Boolean>> filter = (r => r.Id == schedule.Id);
            Schedule schedul = sr.GetFilters(filter).FirstOrDefault();

            sr.CurrentContext.Schedule.DeleteObject(schedule);
            sr.CurrentContext.SaveChanges();
            return true;
        }




        public List<Notification> GetListNotificationsNotReadByIdSchedule(int IdSchedule)
        {
            INotificationBL nbl = new NotificationBL();
            Expression<Func<Notification, Boolean>> filter2 = e => e.IdSchedule == IdSchedule && !e.IsRead;
            return nbl.GetList(filter2).ToList();
        }

        public bool UpdateDndByModal(Schedule schedule)
        {
            ScheduleRepository sr = new ScheduleRepository();
            Expression<Func<Schedule, Boolean>> filter = (r => r.Id == schedule.Id);
            Schedule schedul = sr.GetFilters(filter).FirstOrDefault();
            // TODO: PÀRTIAL
           // DateTime dateTime_planned = DateTime.Parse(schedule.DayPlan + "/" + schedule.MonthPlan + "/" + DateTime.Parse(schedule.DatePlanned.ToString()).Year + " " + schedule.TimePlan);
          //  schedul.DatePlanned = dateTime_planned;
            schedul.UltModUser = schedule.UltModUser;
            return Update(schedul);
        }


        public bool UpdateByModal(Schedule schedule)
        {
            ScheduleRepository sr = new ScheduleRepository();
            Expression<Func<Schedule, Boolean>> filter = (r => r.Id == schedule.Id);
            Schedule schedul = sr.GetFilters(filter).FirstOrDefault();
            schedul.IdScheduleStates = schedule.IdScheduleStates;
            schedul.IdScheduleTypes = schedule.IdScheduleTypes;
            schedul.IdUserAssigned = schedule.IdUserAssigned;
            schedul.IdSchedulePriorities = schedule.IdSchedulePriorities;
            schedul.avis = schedule.avis;
            schedul.Note = schedule.Note;

            // TODO: PÀRTIAL
            //DateTime dateTime_planned = DateTime.Parse(schedule.DayPlan + "/" + schedule.MonthPlan + "/" + DateTime.Parse(schedule.DatePlanned.ToString()).Year + " " + schedule.TimePlan);
           // schedul.DatePlanned = dateTime_planned;
            return Update(schedul);
        }


        /// <summary>
        /// Update task
        /// </summary>
        /// <param name="schedule">object</param>
        /// <returns></returns>
        public bool Update(Schedule schedule)
        {
            ScheduleRepository sr = new ScheduleRepository();
            Expression<Func<Schedule, Boolean>> filter = (r => r.Id == schedule.Id);
            Schedule schedul = sr.GetFilters(filter).FirstOrDefault();

            //si se detecta un cambio de estado se crea anotacion 
            if (schedul.IdScheduleStates != schedule.IdScheduleStates)
            {
                IAnnotationsBL abl = new AnnotationsBL();
                schedul.IdScheduleStates = schedule.IdScheduleStates;
                schedul.UltModUser = schedule.UltModUser;
                bool res = abl.CreateAnnotationBySchedule(schedul);
            }

            if (schedul.avis != schedule.avis)
            {
                if ((bool)schedule.avis)
                {
                    INotificationBL nbl = new NotificationBL();
                    nbl.CreateNotificationBySchedule(schedule);
                }
                else
                {
                    INotificationBL nbl = new NotificationBL();
                    List<Notification> notif = GetListNotificationsNotReadByIdSchedule(schedule.Id);
                    if (notif.Any())
                    {
                        notif.ForEach(x => nbl.Delete(x));
                    }
                }
            }

            if (schedul.Note != schedule.Note)
            {
                INotificationBL nbl = new NotificationBL();

                List<Notification> notif = GetListNotificationsNotReadByIdSchedule(schedule.Id);
                if (notif.Any())
                {
                    notif.ForEach(x => nbl.UpdateDescription(x, schedule.Note));
                }
            }

            if (schedul.DatePlanned != schedule.DatePlanned)
            {
                INotificationBL nbl = new NotificationBL();

                List<Notification> notif = GetListNotificationsNotReadByIdSchedule(schedule.Id);
                if (notif.Any())
                {
                    notif.ForEach(x => nbl.UpdateDateNotification(x,(DateTime)schedule.DatePlanned));
                }
            }

            schedul.IdScheduleEntities = schedule.IdScheduleEntities;
            schedul.CodEntity = schedule.CodEntity;
            schedul.IdScheduleStates = schedule.IdScheduleStates;
            schedul.IdScheduleTypes = schedule.IdScheduleTypes;
            schedul.IdUserCreated = schedule.IdUserCreated;
            schedul.IdUserAssigned = schedule.IdUserAssigned;
            schedul.IdSchedulePriorities = schedule.IdSchedulePriorities;
            schedul.Note = schedule.Note;
            schedul.avis = schedule.avis;
            schedul.DateCreation = schedule.DateCreation;
            schedul.DatePlanned = schedule.DatePlanned;
            schedul.UltModProcess = schedule.UltModProcess;
            schedul.UltModUser = schedule.UltModUser;
            schedul.UltModTimeStamp = schedule.UltModTimeStamp;
            schedul.UltInsUser = schedule.UltInsUser;
            schedul.UltInsTimeStamp = schedule.UltInsTimeStamp;

            sr.CurrentContext.Schedule.Attach(schedul);
            sr.CurrentContext.ObjectStateManager.ChangeObjectState(schedul, System.Data.Entity.EntityState.Modified);
            sr.CurrentContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// Create task
        /// </summary>
        /// <param name="EntityType"></param>
        /// <param name="CodEntity"></param>
        /// <param name="Type"></param>
        /// <param name="State"></param>
        /// <param name="UserCreated"></param>
        /// <param name="UserAssigned"></param>
        /// <param name="Priority"></param>
        /// <param name="Note"></param>
        /// <param name="Avis"></param>
        /// <param name="DatePlanned"></param>
        /// <param name="DateCreation"></param>
        public void CreateTask(int EntityType, int CodEntity, int Type, int State, string UserCreated, string UserAssigned, int Priority, string Note, bool Avis, DateTime DatePlanned, DateTime DateCreation)
        {
            ScheduleRepository sr = new ScheduleRepository();
            Schedule schedul = new Schedule();
            schedul.IdScheduleEntities = EntityType;
            schedul.CodEntity = CodEntity;
            schedul.IdScheduleStates = State;
            schedul.IdScheduleTypes = Type;
            schedul.IdUserCreated = UserCreated;
            schedul.IdUserAssigned = UserAssigned;
            schedul.IdSchedulePriorities = Priority;
            schedul.Note = Note;
            schedul.avis = Avis;
            schedul.DateCreation = DateCreation;
            schedul.DatePlanned = DatePlanned;
            schedul.UltInsUser = UserCreated;
            Add(schedul);
        }

        /// <summary>
        /// return list for calendar view
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<Schedule> GetGrid(Expression<Func<Schedule, Boolean>> filter)
        {
            ScheduleRepository sr = new ScheduleRepository();
            sr.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
            sr.CurrentContext.ContextOptions.LazyLoadingEnabled = true;

            List<Schedule> schedule = sr.GetFilters(filter).ToList();
            List<Schedule> scheduleList = new List<Schedule>();


            foreach (var s in schedule)
            {
                Schedule ss = new Schedule();
                ss.Id = s.Id;
                ss.IdScheduleEntities = s.IdScheduleEntities;
                ss.CodEntity = s.CodEntity;
                ss.IdScheduleStates = s.IdScheduleStates;
                ss.IdScheduleTypes = s.IdScheduleTypes;
                ss.IdUserCreated = s.IdUserCreated;
                ss.IdUserAssigned = s.IdUserAssigned;
                ss.IdSchedulePriorities = s.IdSchedulePriorities;
                ss.Note = s.Note;
                ss.avis = s.avis;
                ss.DateCreation = s.DateCreation;
                ss.DatePlanned = s.DatePlanned;
                //TODO: PARTIALS
                /*
                ss.IdSchedulePrioritiesDesc = s.SchedulePriorities.Description;
                ss.IdScheduleStatesDesc = s.ScheduleStates.Description;
                ss.IdScheduleTypesDesc = s.ScheduleTypes.Description;
                ss.IdUserCreatedDesc = s.AspNetUsers.UserName;
                ss.IdUserAssignedDesc = s.AspNetUsers1.UserName;
                ss.IdSchedulePrioritiesTranslate = s.SchedulePriorities.Translation;
                ss.IdScheduleStatesTranslate = s.ScheduleStates.Translation;
                ss.IdScheduleTypesTranslate = s.ScheduleTypes.Translation;
              */
                scheduleList.Add(ss);
            }
              
            return scheduleList;
        }

        /// <summary>
        /// Get list of results for grid and modals
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Schedule> GetListCompleted(Expression<Func<Schedule, Boolean>> filter)
        {
            ScheduleRepository sr = new ScheduleRepository();
            sr.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
            sr.CurrentContext.ContextOptions.LazyLoadingEnabled = true;

            List<Schedule> schedule = sr.GetFilters(filter).ToList();
            List<Schedule> scheduleList = new List<Schedule>();
            foreach (var s in schedule)
            {
                Schedule ss = new Schedule();
                ss.Id = s.Id;
                ss.IdScheduleEntities = s.IdScheduleEntities;
                ss.CodEntity = s.CodEntity;
                ss.IdScheduleStates = s.IdScheduleStates;
                ss.IdScheduleTypes = s.IdScheduleTypes;
                ss.IdUserCreated = s.IdUserCreated;
                ss.IdUserAssigned = s.IdUserAssigned;
                ss.IdSchedulePriorities = s.IdSchedulePriorities;
                ss.Note = s.Note;
                ss.avis = s.avis;
                ss.DateCreation = s.DateCreation;
                ss.DatePlanned = s.DatePlanned;
                //TODO: PARTIALS
            /*    ss.IdSchedulePrioritiesDesc = s.SchedulePriorities.Description;
                ss.IdScheduleStatesDesc = s.ScheduleStates.Description;
                ss.IdScheduleTypesDesc = s.ScheduleTypes.Description;
                ss.IdUserCreatedDesc = s.AspNetUsers.UserName;
                ss.IdUserAssignedDesc = s.AspNetUsers1.UserName;
                ss.IdSchedulePrioritiesTranslate = s.SchedulePriorities.Translation;
                ss.IdScheduleStatesTranslate = s.ScheduleStates.Translation;
                ss.IdScheduleTypesTranslate = s.ScheduleTypes.Translation;*/

                StudioRepository str = new StudioRepository();
                str.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
                str.CurrentContext.ContextOptions.LazyLoadingEnabled = true;
                Expression<Func<Studio, Boolean>> filter2 = (r => r.Id == s.CodEntity);
                Studio st = str.GetFilters(filter2).FirstOrDefault();
                ClientBL cbl = new ClientBL();
                //TODO: PARTIALS
                // ss.IdPersonDesc = cbl.GetFullName(st != null ? st.Person.Name : string.Empty, st != null ? st.Person.Surname1 : string.Empty, st != null ? st.Person.Surname2 : string.Empty);

                scheduleList.Add(ss);
            }
            return scheduleList;
        }

        public string GetNoteScheduleByIdScheduleSubTypes(int IdScheduleSubTypes)
        {
            ScheduleNotesRepository snr = new ScheduleNotesRepository();
            Expression<Func<ScheduleNotes, Boolean>> filter = (e => e.IdScheduleSubTypes == (int)ScheduleNotesEnum.Pendentderebrerespostadelasolicitut);
            ScheduleNotes scheduleNote = snr.GetFilters(filter).FirstOrDefault();
            return scheduleNote.Note;
        }
    }
}
