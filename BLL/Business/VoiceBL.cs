﻿using BLL.IBusiness;
using DAL.Utils;
using System;

namespace BLL.Business
{
    public class VoiceBL : IVoiceBL
    {
        public bool MakeCall(string extension, string numberphone, string apiKey, string urlBase)
        {
            try
            {
                VoiceService service = new VoiceService();

                return service.MakeCall(extension, numberphone,apiKey,urlBase);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
