﻿using BLL.IBusiness;
using DAL.Entity;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Business
{
    public class RolesBL : IRolesBL
    {
        //  private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public AspNetRoles Get(Expression<Func<AspNetRoles, Boolean>> filter)
        {
            AspNetRolesRepository rr = new AspNetRolesRepository();

            try
            {
                return rr.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<int> GetIdRoleByName(List<string> name)
        {

            AspNetRolesRepository rr = new AspNetRolesRepository();

            try
            {
                List<int> idRoleList = new List<int>();

                Expression<Func<AspNetRoles, bool>> filter = e => true;
                List<AspNetRoles> listaAll = rr.GetFilters(filter);

                foreach (var nameRole in name)
                {
                    AspNetRoles ent = listaAll.Where(p => p.Name == nameRole).FirstOrDefault();

                    if (ent != null)
                        idRoleList.Add(ent.IdRole);
                }

                return idRoleList;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<AspNetRoles> GetList(Expression<Func<AspNetRoles, Boolean>> filter)
        {
            AspNetRolesRepository rr = new AspNetRolesRepository();

            try
            {
                return rr.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //     log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        public AspNetUsers Get(Expression<Func<AspNetUsers, Boolean>> filter)
        {
            AspNetRolesRepository rr = new AspNetRolesRepository();

            try
            {
                AspNetUsersRepository ar = new AspNetUsersRepository();
                return ar.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

    }
}
