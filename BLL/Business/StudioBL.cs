﻿using DAL.Entity;
using DAL.Enums;
using DAL.Repository;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace BLL.Business
{
    public class StudioBL
    {
        // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Get studios list
        /// </summary>
        /// <returns>List<VStudio></returns>
        public List<VStudio> GetStudiosList()
        {
            VStudioRepository vsRep = new VStudioRepository();

            try
            {
                Expression<Func<VStudio, bool>> filter = e => true;
                return vsRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //      log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get causas list
        /// </summary>
        /// <returns>List<VCausas></returns>
        public List<VCausas> GetCausesList()
        {
            VCausasRepository vsRep = new VCausasRepository();

            try
            {
                Expression<Func<VCausas, bool>> filter = e => true;
                return vsRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //   log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get causas list
        /// </summary>
        /// <returns>List<VCausas></returns>
        public List<VCNF_Sex> GetSexesList()
        {
            VCNF_SexRepository vsRep = new VCNF_SexRepository();

            try
            {
                Expression<Func<VCNF_Sex, bool>> filter = e => true;
                return vsRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //   log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<VCNF_Ramo> GetRamosList()
        {
            VCNF_RamoRepository vsRep = new VCNF_RamoRepository();

            try
            {
                Expression<Func<VCNF_Ramo, bool>> filter = e => true;
                return vsRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<VCNF_Periodicity> GetPeriodicityList()
        {
            VCNF_PeriodicityRepository vsRep = new VCNF_PeriodicityRepository();

            try
            {
                Expression<Func<VCNF_Periodicity, bool>> filter = e => true;
                return vsRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //   log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get causas list
        /// </summary>
        /// <returns>List<VMotivoPropuestaRamo></returns>
        public List<VMotivoPropuestaRamo> GetMotivoPropuestasList()
        {
            VMotivoPropuestaRamoRepository vsRep = new VMotivoPropuestaRamoRepository();

            try
            {
                Expression<Func<VMotivoPropuestaRamo, bool>> filter = e => true;
                return vsRep.GetFilters(filter).OrderBy(p => p.Descripcion).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get studios list
        /// </summary>
        /// <returns>List<VStudio></returns>
        public List<VStudio> GetStudiosListByProcessorId(int proccessorId,bool all)
        {
            VStudioRepository vsRep = new VStudioRepository();

            try
            {
                Expression<Func<VStudio, bool>> filter = null;

                if (!all) filter = e => e.ProcessorId == proccessorId && e.StateId == (int)StateEnum.Active;
                else filter = e => e.ProcessorId == proccessorId;

                List<VStudio> list = new List<VStudio>();
                list = vsRep.GetFilters(filter).ToList();
                return list;
            }
            catch (HttpResponseException ex)
            {
                //   log.Error(ex.InnerException);                
                //    log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<VStudio> GetStudiosListByClientId(int clientId, bool all)
        {
            VStudioRepository vsRep = new VStudioRepository();

            try
            {
                
                Expression<Func<VStudio, bool>> filter = null;

                if (!all) filter = (e => e.ClientId == clientId && e.StateId == (int)StateEnum.Active);
                else filter = (e => e.ClientId == clientId);

                List<VStudio> list = new List<VStudio>();
                list = vsRep.GetFilters(filter).ToList();

                return list;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public FullStudio GetStudioById(int studioId)
        {
            try
            {
                StudioRepository sRep = new StudioRepository();
                StudioProposalBL spBL = new StudioProposalBL();

                FullStudio fullStudio = new FullStudio();
                if (studioId > 0)
                {
                    using (TransactionScope tran = new TransactionScope())
                    {
                        try
                        {
                            Expression<Func<Studio, bool>> filterStudio = e => e.Id == studioId;
                            fullStudio.Studio = sRep.GetFilters(filterStudio).FirstOrDefault();
                            fullStudio.Proposals = spBL.GetStudioProposalByStudioId(studioId);

                            if (fullStudio.Studio != null && fullStudio.Studio.ClientId > 0)
                            {
                                PersonRepository pRep = new PersonRepository();
                                Expression<Func<Person, bool>> filterClient = e => e.Id == fullStudio.Studio.ClientId;
                                fullStudio.Client = pRep.GetFilters(filterClient).FirstOrDefault();

                                VPersonCollectivesRepository pcRep = new VPersonCollectivesRepository();
                                Expression<Func<VPersonCollectives, bool>> filterCols = e => e.IdPerson == fullStudio.Studio.ClientId && e.DateDelete == null;
                                fullStudio.ClientCollectives = pcRep.GetFilters(filterCols).ToList();

                            }
                            tran.Complete();
                            return fullStudio;
                        }
                        catch (Exception ex)
                        {
                            ////log.Error(ex.Message);
                            tran.Dispose();
                            throw new Exception();
                        }
                    }
                }
                else return fullStudio;
            }
            catch (HttpResponseException ex)
            {
                //     log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Add new entity
        /// </summary>
        /// <param name="studio">object of entity</param>
        public FullStudio Add(FullStudio study)
        {
            StudioRepository sRep = new StudioRepository();
            StudioProposalBL spBL = new StudioProposalBL();
            ClientBL cBL = new ClientBL();
            DateTime _now = DateTime.UtcNow;
            // TODO: GET CURRENT USER
            int currentUser = 1;

            try
            {

                Studio studio = study.Studio;

                if (studio == null)
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("Add Studio: studio = null") });

                List<StudioProposal> proposals = study.Proposals;
                Person client = study.Client;

                using (TransactionScope tran = new TransactionScope())
                {
                    try
                    { 
                        if (client.Id == 0)
                        {
                            Person tmpClient = cBL.Add(client, study.ClientCollectives);
                            studio.ClientId = tmpClient.Id;
                            if (tmpClient == null) 
                                throw new Exception();
                        }
                        else
                        {
                            studio.ClientId = client.Id;
                        }

                        studio.DateCreate = _now;
                        studio.DateUpdate = _now;
                        studio.CreatedBy = currentUser;
                        studio.UpdatedBy = currentUser;
                        studio.StateId = (int)StudioStateEnum.Pending;
                        studio.SubAgentId = studio.SubAgentId == 0 ? null : studio.SubAgentId; 

                        sRep.CurrentContext.Studio.AddObject(studio);
                        sRep.CurrentContext.SaveChanges();
                        if (studio == null) 
                            throw new Exception();

                        if (proposals != null && proposals.Count > 0)
                        {
                            foreach(StudioProposal proposal in proposals)
                            {
                                proposal.StudioId = studio.Id;
                                proposal.CreatedBy = currentUser;
                                proposal.UpdatedBy = currentUser;
                                proposal.DateCreate = _now;
                                proposal.DateUpdate = _now;
                                proposal.StateId = (int)StudioProposalStateEnum.Pending;
                                StudioProposal prop = spBL.Add(proposal);

                                proposal.Id = prop.Id;

                                if (prop == null) 
                                    throw new Exception();
                            }
                        }

                        /*
                         * CREACIÓ DE NOVA TASCA I NOVA ANOTACIÓ AL CREAR UN NOU ESTUDI
                        IUsersBL ubl = new UsersBL();
                        IAnnotationsBL abl = new AnnotationsBL();
                        abl.CreateAnnotation((int)ScheduleEntitiesEnum.Estudi, studio.Id, (int)AnnotationsTypesEnum.cambioEstado, (int)AnnotationsSubtypesEnum.abierto, (int)AnnotationsStatesEnum.hecho,
                                           ubl.Fake_GetUser(), "Estudi creat", DateTime.Now, DateTime.Now);

                        IScheduleBL sbl = new ScheduleBL();
                        sbl.CreateTask((int)ScheduleEntitiesEnum.Estudi, studio.Id, (int)ScheduleTypesEnum.Seguiment, (int)ScheduleStatesEnum.pendiente, ubl.Fake_GetUser(), ubl.Fake_GetUser(),
                                            "mail@mail.com", (int)SchedulePrioritiesEnum.Baixa, "Tasca nova creació: Tasca1", true, DateTime.Now.AddDays(2), DateTime.Now);
                         */

                        tran.Complete();
                        return study;
                    }
                    catch (Exception ex)
                    {
                        //log.Error(ex.Message);
                        tran.Dispose();
                        throw new Exception();
                    }
                }
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <returns></returns>
        public FullStudio Update(FullStudio study)
        {
            StudioRepository sRep = new StudioRepository();
            StudioProposalBL spBL = new StudioProposalBL();

            try
            {
                Studio studio = study.Studio;

                if (studio == null)
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("Update Studio: studio = null") });

                List<StudioProposal> proposals = study.Proposals;
                Person client = study.Client;
                DateTime _now = DateTime.UtcNow;
                // TODO: GET CURRENT USER
                int currentUser = 1;

                using (TransactionScope tran = new TransactionScope())
                {
                    try
                    {
                        studio.DateUpdate = DateTime.UtcNow;
                        studio.UpdatedBy = currentUser;
                        studio.ClientId = study.Client.Id;
                        studio.SubAgentId = studio.SubAgentId == 0 ? null : studio.SubAgentId;

                        sRep.CurrentContext.Studio.Attach(studio);
                        sRep.CurrentContext.ObjectStateManager.ChangeObjectState(studio, System.Data.Entity.EntityState.Modified);
                        sRep.CurrentContext.SaveChanges();

                        List<StudioProposal> oldProposals = spBL.GetStudioProposalByStudioId(studio.Id);
                        List<StudioProposal> propsToInsert = proposals.Where(a => a.Id == 0).ToList();
                        List<StudioProposal> propsToUpdate = proposals.Where(a => a.Id != 0 && oldProposals.Any(b => b.Id == a.Id)).ToList();
                        List<StudioProposal> propsToDelete = oldProposals.Where(a => !proposals.Any(b => b.Id > 0 && b.Id == a.Id)).ToList();

                        if (propsToInsert != null && propsToInsert.Count > 0)
                        {
                            foreach (StudioProposal proposal in propsToInsert)
                            {
                                proposal.StudioId = studio.Id;
                                proposal.CreatedBy = currentUser;
                                proposal.UpdatedBy = currentUser;
                                proposal.DateCreate = _now;
                                proposal.DateUpdate = _now;
                                proposal.StateId = (int)StudioProposalStateEnum.Pending;
                                StudioProposal tmpProp = spBL.Add(proposal);
                                if (tmpProp == null) throw new Exception();
                            }
                        }

                        if (propsToUpdate != null && propsToUpdate.Count > 0)
                        {
                            foreach (StudioProposal proposal in propsToUpdate)
                            {
                                proposal.UpdatedBy = currentUser;
                                proposal.DateUpdate = _now;
                                proposal.CreatedBy = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().CreatedBy;
                                proposal.DateCreate = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().DateCreate;
                                proposal.StateId = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().StateId;
                                StudioProposal tmpProp = spBL.Update(proposal);
                                if (tmpProp == null) throw new Exception();
                            }
                        }

                        if (propsToDelete != null && propsToDelete.Count > 0)
                        {
                            foreach (StudioProposal proposal in propsToDelete)
                            {
                                proposal.CreatedBy = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().CreatedBy;
                                proposal.DateCreate = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().DateCreate;
                                proposal.UpdatedBy = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().UpdatedBy;
                                proposal.DateUpdate = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().DateUpdate;
                                proposal.StateId = oldProposals.Where(a => a.Id == proposal.Id).FirstOrDefault().StateId;
                                proposal.DeletedBy = currentUser;
                                proposal.DateDelete = _now;
                                StudioProposal tmpProp = spBL.Delete(proposal);
                                if (tmpProp == null) throw new Exception();
                            }
                        }

                        tran.Complete();
                        return study;
                    }
                    catch (Exception ex)
                    {
                        //log.Error(ex.Message);
                        tran.Dispose();
                        throw new Exception();
                    }
                }
            }
            catch (HttpResponseException ex)
            {
                //   log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //    log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <returns></returns>
        public bool SetStudioState(int studioId, StudioStateEnum stateId, int? cancelOption)
        {
            StudioRepository sRep = new StudioRepository();
            try
            {
                Expression<Func<Studio, bool>> filterStudio = e => e.Id == studioId;
                Studio studio = sRep.GetFilters(filterStudio).FirstOrDefault();
                // TODO: GET CURRENT USER
                int currentUser = 1;

                if (studio != null)
                {
                    if (stateId == StudioStateEnum.Cancelled)
                        studio.MotivoBajaId = cancelOption;
                    else if (stateId == StudioStateEnum.Pending)
                        studio.MotivoBajaId = null;

                    studio.StateId = (int)stateId;
                    studio.DateUpdate = DateTime.UtcNow;
                    studio.UpdatedBy = currentUser;

                    sRep.CurrentContext.Studio.Attach(studio);
                    sRep.CurrentContext.ObjectStateManager.ChangeObjectState(studio, System.Data.Entity.EntityState.Modified);
                    sRep.CurrentContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

      
        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Studio> GetListWithRelations(Expression<Func<Studio, Boolean>> filter)
        {
            try
            {
                StudioRepository sr = new StudioRepository();
                sr.CurrentContext.ContextOptions.ProxyCreationEnabled = true;
                sr.CurrentContext.ContextOptions.LazyLoadingEnabled = true;
                return sr.GetFilters(filter).ToList();
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Studio> GetList(Expression<Func<Studio, Boolean>> filter)
        {
            try
            {
                StudioRepository sr = new StudioRepository();
                return sr.GetFilters(filter).ToList();
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<AutoCompleteModel> GetAutocompleteStudioList(List<Studio> ListStudio)
        {
            try
            {
                List<AutoCompleteModel> clientsList = new List<AutoCompleteModel>();
                foreach (var element in ListStudio)
                {
                    AutoCompleteModel newEle = new AutoCompleteModel();
                    newEle.id = element.Id;
                    newEle.name = element.Id.ToString();
                    clientsList.Add(newEle);
                }
                return clientsList;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }


        public List<AutoCompleteModel> GetAutocompleteStudioListWithFullName(List<Studio> ListStudio)
        {
            try
            {
                ClientBL cbl = new ClientBL();


                List<AutoCompleteModel> clientsList = new List<AutoCompleteModel>();
                foreach (var element in ListStudio)
                {
                    AutoCompleteModel newEle = new AutoCompleteModel();
                    newEle.id = element.Id;
                    newEle.name = element.Id.ToString() + " - " + cbl.GetFullName(element.Person != null ? element.Person.Name : string.Empty, 
                                                                            element.Person != null ? element.Person.Surname1 : string.Empty, 
                                                                            element.Person != null ? element.Person.Surname2 : string.Empty);
                    clientsList.Add(newEle);
                }
                return clientsList;
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }


        /// <summary>
        /// Get causas list
        /// </summary>
        /// <returns>List<VMotivoPropuestaRamo></returns>
        public List<MotivoBaja> GetMotivoBajaList()
        {
            MotivoBajaRepository mbRep = new MotivoBajaRepository();

            try
            {
                Expression<Func<MotivoBaja, bool>> filter = e => true;
                return mbRep.GetFilters(filter).OrderBy(p => p.Descripcion).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

    }
}
