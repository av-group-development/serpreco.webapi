﻿
using DAL.Entity;
using DAL.Repository;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Business
{
    public class ClientBL
    {
        // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string GetFullName(string Name, string Surname1, string Surname2)
        {
            string newName = !string.IsNullOrEmpty(Name) ? ", " + Name : "";
            return ((Surname1 + " " + Surname2).Trim() + newName);
                 
        }

        public List<AutoCompleteModel> GetAutocompleteClientsList()
        {
            try
            {
                List<Person> tmpClientsList = GetClientsList();
                List<AutoCompleteModel> clientsList = new List<AutoCompleteModel>();
                foreach (Person element in tmpClientsList)
                {
                    AutoCompleteModel newEle = new AutoCompleteModel();
                    newEle.id = element.Id;
                    newEle.name = GetFullName(element.Name, element.Surname1, element.Surname2);
                    clientsList.Add(newEle);
                }
                return clientsList;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<Person> GetFullClientsList()
        {
            try
            {
                List<Person> clientsList = GetClientsList();
                return clientsList;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get studios list
        /// </summary>
        /// <returns>List<VStudio></returns>
        public Person GetClientById(int clientId)
        {
            PersonRepository pRep = new PersonRepository();

            try
            {
                Expression<Func<Person, bool>> filter = e => e.Id == clientId;
                return pRep.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get client collectives
        /// </summary>
        /// <returns>List<VStudio></returns>
        public List<PersonCollectives> GetClientCollectivesByClientId(int clientId)
        {
            PersonCollectivesRepository pcRep = new PersonCollectivesRepository();

            try
            {
                Expression<Func<PersonCollectives, bool>> filter = e => e.IdPerson == clientId;
                return pcRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }


        /// <summary>
        /// Add new entity
        /// </summary>
        public Person Add(Person client, List<VPersonCollectives> clientCollectives)
        {
            PersonRepository pRep = new PersonRepository();

            try
            {
                // TODO: GET CURRENT USER
                int currentUser = 1;
                // TODO: Asignar SPerson autonumerico cuando lo sepamos
                client.Sperson = "11";
                // TODO: Mirar el IdLanguage, los hardcodeados son por que llegas vacíos, hay que hablarlo
                client.IdLanguage = 2;
                client.IdTypes = 8;
                client.DateCreate = DateTime.UtcNow;
                client.DateUpdate = DateTime.UtcNow;
                if (client.DocumentNumber == null) client.DocumentNumber = "";
                if (client.IdDocumentType == 0) client.IdDocumentType = null;
                if (client.IdSex == 0) client.IdSex = null;
                pRep.CurrentContext.Person.AddObject(client);
                pRep.CurrentContext.SaveChanges();
                SetClientCollectives(client.Id, GetCollectivesIds(clientCollectives), currentUser);

                return client;
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <returns></returns>
        public Person Update(Person client, List<VPersonCollectives> clientCollectives)
        {
            PersonRepository pRep = new PersonRepository();

            try
            {
                // TODO: GET CURRENT USER
                int currentUser = 1;
                client.DateUpdate = DateTime.UtcNow;
                if (client.IdDocumentType == 0) client.IdDocumentType = null;
                if (client.IdSex == 0) client.IdSex = null;
                pRep.CurrentContext.Person.Attach(client);
                pRep.CurrentContext.ObjectStateManager.ChangeObjectState(client, System.Data.Entity.EntityState.Modified);
                pRep.CurrentContext.SaveChanges();
                SetClientCollectives(client.Id, GetCollectivesIds(clientCollectives), currentUser);

                return client;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<VPersonCollectives> GetClientCollectives(int clientId)
        {
            VPersonCollectivesRepository pcR = new VPersonCollectivesRepository();
            try
            {
                Expression<Func<VPersonCollectives, bool>> filter = e => e.IdPerson == clientId;
                List<VPersonCollectives> list = pcR.GetFilters(filter).ToList();
                return list;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //  log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public List<PersonCollectives> SetClientCollectives(int clientId, List<int> collectives, int currentUser)
        {
            PersonCollectivesRepository pcR = new PersonCollectivesRepository();
            DateTime _now = DateTime.UtcNow;
            try
            {
                Expression<Func<PersonCollectives, bool>> filter = e => e.IdPerson == clientId && e.DateDelete == null;
                List<PersonCollectives> currentPcList = pcR.GetFilters(filter).ToList();

                foreach (PersonCollectives pc in currentPcList)
                {
                    if (!collectives.Any(item => item == pc.IdCollective))
                    {
                        pc.DateDelete = _now;
                        pc.DeletedBy = currentUser;
                        pcR.CurrentContext.ObjectStateManager.ChangeObjectState(pc, System.Data.Entity.EntityState.Modified);
                        pcR.CurrentContext.SaveChanges();
                    }
                }

                List<PersonCollectives> newPcList = new List<PersonCollectives>();
                foreach (int col in collectives)
                {
                    if (!currentPcList.Any(item => item.IdCollective == col))
                    {
                        PersonCollectives tmpPerCol = new PersonCollectives();
                        tmpPerCol.IdCollective = col;
                        tmpPerCol.IdPerson = clientId;
                        tmpPerCol.CreatedBy = currentUser;
                        tmpPerCol.DateCreate = _now;
                        pcR.CurrentContext.PersonCollectives.AddObject(tmpPerCol);
                        pcR.CurrentContext.SaveChanges();
                        newPcList.Add(tmpPerCol);
                    }
                }
                return newPcList;
            }
            catch (HttpResponseException ex)
            {
                // log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        #region "************************* PRIVATE *************************"

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private List<Person> GetClientsList()
        {
            PersonRepository pRep = new PersonRepository();

            try
            {
                Expression<Func<Person, bool>> filter = e => true;
                return pRep.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        private List<int> GetCollectivesIds(List<VPersonCollectives> vpc)
        {
            List<int> colsList = new List<int>();
            foreach (VPersonCollectives pc in vpc)
            {
                colsList.Add(pc.IdCollective);
            }
            return colsList;
        }

        #endregion

    }
}
