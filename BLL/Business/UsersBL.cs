﻿using DAL.Entity;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using BLL.IBusiness;

namespace BLL.Business
{
    public class UsersBL : IUsersBL
    {
      //  private static readonly log4net.ILog log =log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

     /*   public IdentityUser AddUser(IdentityUser user,string pass)
        {
            try
            {
                // Default UserStore constructor uses the default connection string named: DefaultConnection
                var userStore = new UserStore<IdentityUser>();
                var manager = new UserManager<IdentityUser>(userStore);

                IdentityResult result = manager.Create(user, pass);

                if (result.Succeeded == false)
                {
                    foreach (var r in result.Errors)
                    {
                        log.Info(r);
                    }
                }
                return result.Succeeded == true ? user : null;
            }
            catch (HttpResponseException ex)
            {
                log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
        */
        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public AspNetUsers Get(Expression<Func<AspNetUsers, Boolean>> filter)
        {
            try
            {
                AspNetUsersRepository ar = new AspNetUsersRepository();
                return ar.GetFilters(filter).FirstOrDefault();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<AspNetUsers> GetList(Expression<Func<AspNetUsers, Boolean>> filter)
        {
            try
            {
                AspNetUsersRepository ar = new AspNetUsersRepository();
                return ar.GetFilters(filter).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public string Fake_GetUser()
        {
            try
            {
                Expression<Func<AspNetUsers, Boolean>> filter = e =>true;
                AspNetUsersRepository ar = new AspNetUsersRepository();
                AspNetUsers user =  ar.GetFilters(filter).FirstOrDefault();
                return user.Id;
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        public AspNetUsers Update(AspNetUsers ent)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    try
                    {
                        AspNetUsersRepository repo = new AspNetUsersRepository();
                        //ent.DateUpdate = DateTime.UtcNow;

                            repo.CurrentContext.AspNetUsers.Attach(ent);
                            repo.CurrentContext.ObjectStateManager.ChangeObjectState(ent, System.Data.Entity.EntityState.Modified);
                            repo.CurrentContext.SaveChanges();
                            tran.Complete();

                            return ent;
                    }
                    catch (Exception ex)
                    {
                        //log.Error(ex.Message);
                        tran.Dispose();
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
                    }
                }
                
            }
            catch (HttpResponseException ex)
            {
                // log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                // log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

    }
}
