﻿using BLL.IBusiness;
using DAL.Entity;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Business
{
    public class PrescriptorsBL : IPrescriptorsBL
    {
       // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Prescriptors> GetList()
        {

            try
            {
                Expression<Func<Prescriptors, bool>> filterAll = e => e.Id > 0;
                PrescriptorsRepository repo = new PrescriptorsRepository();

                return repo.GetFilters(filterAll).OrderBy(p => p.Nombre).ToList();
            }
            catch (HttpResponseException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //   log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
    }
}
