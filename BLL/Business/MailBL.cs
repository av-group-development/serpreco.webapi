﻿using BLL.IBusiness;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;

namespace BLL.Business
{
    public class MailBL : IMailBL
    {
       // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SendSimpleMail(string to, string Subject, string Message, string from, List<Attach> attach)
        {
            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress(to));
            msg.Bcc.Add(new MailAddress(from));

            //TODO: ConfigurationManager
            /* string mailCredentials = ConfigurationManager.AppSettings.Get("mailCredentials");
             string mailSerpreco = ConfigurationManager.AppSettings.Get("mailSerpreco");
             string mailHost = ConfigurationManager.AppSettings.Get("mailHost");
             string mailPort = ConfigurationManager.AppSettings.Get("mailPort");
             */

            string mailCredentials = "";
            string mailSerpreco = "";
            string mailHost = "";
            string mailPort = "";
            string From = from;  
            string NameFrom = from;
            Encoding FromEncoding = Encoding.UTF8;

            msg.From = new MailAddress(From, NameFrom, FromEncoding);
            msg.Subject = Subject;
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = Message;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = true;

            foreach (var f in attach)
            {
                byte[] byteArray = Convert.FromBase64String(f.DocumentBase64);

                MemoryStream ms = new MemoryStream(byteArray);
                msg.Attachments.Add(new Attachment(ms, f.Nombre));
            }

            SmtpClient client = new SmtpClient();
            client.Credentials = new NetworkCredential(mailSerpreco, mailCredentials);
            client.Port = Convert.ToInt32(mailPort);
            client.Host = mailHost;
            client.EnableSsl = true;

            try
            {
                client.Send(msg);
            }
            catch (SmtpException ex)
            {
                //log.Error(ex.Message);
                throw;
            }
            catch (HttpResponseException ex)
            {
                //  log.Error(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                //    log.Error(ex.Message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }
    }
}
