﻿using BLL.IBusiness;
using DAL.Entity;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Business
{
    public class TemplateBL : ITemplate
    {
        /// <summary>
        /// Add new entity
        /// </summary>
        /// <param name="role">object of entity</param>
        public Person Add(Person role)
        {
            try
            {
                PersonRepository rr = new PersonRepository();
                Person newRole = new Person();
                newRole.BirthDate = role.BirthDate;
                newRole.BussinesAdvisor = role.BussinesAdvisor;

                rr.CurrentContext.Person.AddObject(newRole);
                rr.CurrentContext.SaveChanges();

                return role;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public Person Get(Expression<Func<Person, Boolean>> filter)
        {
            try
            {
                PersonRepository rr = new PersonRepository();
                return rr.GetFilters(filter).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Get list of results
        /// </summary>
        /// <param name="filter">param of filter</param>
        /// <returns></returns>
        public List<Person> GetList(Expression<Func<Person, Boolean>> filter)
        {
            try
            {
                PersonRepository rr = new PersonRepository();
                return rr.GetFilters(filter).ToList();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="role">object of entity</param>
        public bool Delete(Person role)
        {
            try
            {
                PersonRepository rr = new PersonRepository();
                Expression<Func<Person, Boolean>> filter = (r => r.Id == role.Id);
                Person rol = rr.GetFilters(filter).FirstOrDefault();

                rr.CurrentContext.Person.DeleteObject(rol);
                rr.CurrentContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <param name="role">object of entity</param>
        /// <returns></returns>
        public bool Update(Person role)
        {
            try
            {
                PersonRepository rr = new PersonRepository();
                Expression<Func<Person, Boolean>> filter = (r => r.Id == role.Id);
                Person rol = rr.GetFilters(filter).FirstOrDefault();
                rol.BirthDate = role.BirthDate;
                rol.BussinesAdvisor = role.BussinesAdvisor;

                rr.CurrentContext.Person.Attach(rol);
                rr.CurrentContext.ObjectStateManager.ChangeObjectState(rol, System.Data.Entity.EntityState.Modified);
                rr.CurrentContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
