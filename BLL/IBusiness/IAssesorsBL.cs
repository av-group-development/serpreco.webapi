﻿using DAL.Entity;
using System.Collections.Generic;


namespace BLL.IBusiness
{
    public interface IAssesorsBL
    {
        List<Assesors> GetList();
    }
}
