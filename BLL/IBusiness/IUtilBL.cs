﻿using System;

namespace BLL.IBusiness
{
    public interface IUtilBL
    {
        //string AccessAnotherConnectionString(string connString, string selectStatement);
        int GetPrevLaboralDayBefore48hByDate(DateTime Date);
    }
}
