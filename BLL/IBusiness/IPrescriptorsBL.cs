﻿using DAL.Entity;
using System.Collections.Generic;

namespace BLL.IBusiness
{
    public interface IPrescriptorsBL
    {
        List<Prescriptors> GetList();
    }
}
