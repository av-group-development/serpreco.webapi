﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BLL.IBusiness
{
    public interface IMasterTablesBL
    {
        ScheduleStates Get(Expression<Func<ScheduleStates, bool>> filter);
        List<ScheduleStates> GetList(Expression<Func<ScheduleStates, bool>> filter);
        AnnotationsStates Get(Expression<Func<AnnotationsStates, bool>> filter);
        List<AnnotationsStates> GetList(Expression<Func<AnnotationsStates, bool>> filter);
        AnnotationsTypes Get(Expression<Func<AnnotationsTypes, bool>> filter);
        List<AnnotationsTypes> GetList(Expression<Func<AnnotationsTypes, bool>> filter);
        AnnotationsSubtypes Get(Expression<Func<AnnotationsSubtypes, bool>> filter);
        List<AnnotationsSubtypes> GetList(Expression<Func<AnnotationsSubtypes, bool>> filter);
        ScheduleTypes Get(Expression<Func<ScheduleTypes, bool>> filter);
        List<ScheduleTypes> GetList(Expression<Func<ScheduleTypes, bool>> filter);
        ScheduleEntities Get(Expression<Func<ScheduleEntities, bool>> filter);
        List<ScheduleEntities> GetList(Expression<Func<ScheduleEntities, bool>> filter);
        SchedulePriorities Get(Expression<Func<SchedulePriorities, bool>> filter);
        List<SchedulePriorities> GetList(Expression<Func<SchedulePriorities, bool>> filter);
    }
}
