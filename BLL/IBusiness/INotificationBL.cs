﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BLL.IBusiness
{
    public interface INotificationBL
    {
        Notification Add(Notification notification);
        Notification Get(Expression<Func<Notification, Boolean>> filter);
        List<Notification> GetList(Expression<Func<Notification, Boolean>> filter);
        bool Update(Notification notification);
        bool Delete(Notification notification);
        bool UpdateIsRead(Notification notification);
        bool CreateNotificationBySchedule(Schedule schedule);
        bool UpdateDescription(Notification notification, string newDescription);
        bool UpdateDateNotification(Notification notification, DateTime dateNotification);
    }
}
