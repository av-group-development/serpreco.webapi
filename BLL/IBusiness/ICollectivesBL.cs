﻿using DAL.Entity;
using System.Collections.Generic;

namespace BLL.IBusiness
{
    public interface ICollectivesBL
    {
        List<CNF_Collectives> GetList();
    }
}
