﻿using DAL.Entity;
using System.Collections.Generic;

namespace BLL.IBusiness
{
    public interface ICompanyBL
    {
        List<Company> GetList();
    }
}
