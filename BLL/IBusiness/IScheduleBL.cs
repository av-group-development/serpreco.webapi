﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BLL.IBusiness
{
    public interface IScheduleBL
    {
        Schedule Add(Schedule schedule);
        Schedule Get(Expression<Func<Schedule, Boolean>> filter);
        List<Schedule> GetList(Expression<Func<Schedule, Boolean>> filter);
        List<Schedule> GetListCompleted(Expression<Func<Schedule, Boolean>> filter);
        bool Delete(Schedule schedule);
        bool Update(Schedule schedule);
        void CreateTask(int EntityType, int CodEntity, int Type, int State, string UserCreated, string UserAssigned, int Priority, string Note, bool Avis, DateTime DatePlanned, DateTime DateCreation);
        List<Schedule> GetGrid(Expression<Func<Schedule, Boolean>> filter);
        bool UpdateByModal(Schedule schedule);
        bool UpdateDndByModal(Schedule schedule);
        string GetNoteScheduleByIdScheduleSubTypes(int IdScheduleSubTypes);
    }
}
