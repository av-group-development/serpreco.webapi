﻿using DAL.Entity;
using System.Collections.Generic;

namespace BLL.IBusiness
{
    public interface IProcessorsBL
    {
        List<Processors> GetList();
    }
}
