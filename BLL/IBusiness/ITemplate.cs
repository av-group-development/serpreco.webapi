﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BLL.IBusiness
{
    public interface ITemplate
    {
        Person Add(Person role);
        Person Get(Expression<Func<Person, Boolean>> filter);
        List<Person> GetList(Expression<Func<Person, Boolean>> filter);
        bool Delete(Person role);
        bool Update(Person role);
    }
}
