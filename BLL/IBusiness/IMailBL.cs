﻿
using DAL.ViewModel;
using System.Collections.Generic;

namespace BLL.IBusiness
{
    public interface IMailBL
    {
        void SendSimpleMail(string to, string Subject, string Message, string from, List<Attach> attach);
    }

}
