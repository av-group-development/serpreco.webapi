﻿using System.Collections.Generic;
using DAL.ViewModel;

namespace BLL.IBusiness
{
    public interface IInvesDocBL
    {
        bool UploadStudioSingleFile(StudioInvesDocumentModelDAL document);

        List<StudioProposalInvesDocumentModel> GetStudioDocuments(int studyId, int proposalId, string sPerson);
    }
}
