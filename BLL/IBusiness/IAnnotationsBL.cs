﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;


namespace BLL.IBusiness
{
    public interface IAnnotationsBL
    {
        Annotations Add(Annotations annotations);
        Annotations Get(Expression<Func<Annotations, Boolean>> filter);
        List<Annotations> GetList(Expression<Func<Annotations, Boolean>> filter);
        List<Annotations> GetListCompleted(Expression<Func<Annotations, Boolean>> filter);
        bool Delete(Annotations annotations);
        bool Update(Annotations annotations);
        List<Annotations> GetGrid(Expression<Func<Annotations, Boolean>> filter);
        bool UpdateByModal(Annotations annotations);

        bool CreateAnnotationBySchedule(Schedule schedule);

    }
}
