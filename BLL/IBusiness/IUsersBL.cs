﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BLL.IBusiness
{
    public interface IUsersBL
    {
       // IdentityUser AddUser(IdentityUser user, string pass);
        string Fake_GetUser();
        AspNetUsers Update(AspNetUsers ent);
        List<AspNetUsers> GetList(Expression<Func<AspNetUsers, Boolean>> filter);
        AspNetUsers Get(Expression<Func<AspNetUsers, Boolean>> filter);
    }
}
