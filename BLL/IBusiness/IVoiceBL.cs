﻿namespace BLL.IBusiness
{
    public interface IVoiceBL
    {
        bool MakeCall(string extension, string numberphone,string apiKey,string urlBase);
    }
}
