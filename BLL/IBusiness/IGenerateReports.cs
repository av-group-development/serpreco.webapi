﻿using DAL.ViewModel;

namespace BLL.IBusiness
{
    public interface IGenerateReports
    {
        DocumentModel GenerateDocumentSolicitud(int proposalId,string idioma);
    }
}
