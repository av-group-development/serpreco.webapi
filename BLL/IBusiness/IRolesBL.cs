﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BLL.IBusiness
{
    public interface IRolesBL
    {
        AspNetRoles Get(Expression<Func<AspNetRoles, Boolean>> filter);
        List<AspNetRoles> GetList(Expression<Func<AspNetRoles, Boolean>> filter);
  
    }
}
