//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace DAL.Entity
{
    public partial class VPersonCollectives
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual int IdPerson
        {
            get;
            set;
        }
    
        public virtual int IdCollective
        {
            get;
            set;
        }
    
        public virtual string Nombre
        {
            get;
            set;
        }
    
        public virtual System.DateTime DateCreate
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DateDelete
        {
            get;
            set;
        }

        #endregion

    }
}
