//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace DAL.Entity
{
    public partial class CNF_Periodicity_Language
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual int Id_CNF_Periodicity
        {
            get { return _id_CNF_Periodicity; }
            set
            {
                if (_id_CNF_Periodicity != value)
                {
                    if (CNF_Periodicity != null && CNF_Periodicity.Id != value)
                    {
                        CNF_Periodicity = null;
                    }
                    _id_CNF_Periodicity = value;
                }
            }
        }
        private int _id_CNF_Periodicity;
    
        public virtual string Idioma
        {
            get;
            set;
        }
    
        public virtual string Description
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual CNF_Periodicity CNF_Periodicity
        {
            get { return _cNF_Periodicity; }
            set
            {
                if (!ReferenceEquals(_cNF_Periodicity, value))
                {
                    var previousValue = _cNF_Periodicity;
                    _cNF_Periodicity = value;
                    FixupCNF_Periodicity(previousValue);
                }
            }
        }
        private CNF_Periodicity _cNF_Periodicity;

        #endregion

        #region Association Fixup
    
        private void FixupCNF_Periodicity(CNF_Periodicity previousValue)
        {
            if (previousValue != null && previousValue.CNF_Periodicity_Language.Contains(this))
            {
                previousValue.CNF_Periodicity_Language.Remove(this);
            }
    
            if (CNF_Periodicity != null)
            {
                if (!CNF_Periodicity.CNF_Periodicity_Language.Contains(this))
                {
                    CNF_Periodicity.CNF_Periodicity_Language.Add(this);
                }
                if (Id_CNF_Periodicity != CNF_Periodicity.Id)
                {
                    Id_CNF_Periodicity = CNF_Periodicity.Id;
                }
            }
        }

        #endregion

    }
}
