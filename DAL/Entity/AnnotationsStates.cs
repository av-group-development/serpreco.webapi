//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace DAL.Entity
{
    public partial class AnnotationsStates
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual string Description
        {
            get;
            set;
        }
    
        public virtual string Translation
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<Annotations> Annotations
        {
            get
            {
                if (_annotations == null)
                {
                    var newCollection = new FixupCollection<Annotations>();
                    newCollection.CollectionChanged += FixupAnnotations;
                    _annotations = newCollection;
                }
                return _annotations;
            }
            set
            {
                if (!ReferenceEquals(_annotations, value))
                {
                    var previousValue = _annotations as FixupCollection<Annotations>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAnnotations;
                    }
                    _annotations = value;
                    var newValue = value as FixupCollection<Annotations>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAnnotations;
                    }
                }
            }
        }
        private ICollection<Annotations> _annotations;
    
        public virtual ICollection<AnnotationsSubtypes> AnnotationsSubtypes
        {
            get
            {
                if (_annotationsSubtypes == null)
                {
                    var newCollection = new FixupCollection<AnnotationsSubtypes>();
                    newCollection.CollectionChanged += FixupAnnotationsSubtypes;
                    _annotationsSubtypes = newCollection;
                }
                return _annotationsSubtypes;
            }
            set
            {
                if (!ReferenceEquals(_annotationsSubtypes, value))
                {
                    var previousValue = _annotationsSubtypes as FixupCollection<AnnotationsSubtypes>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAnnotationsSubtypes;
                    }
                    _annotationsSubtypes = value;
                    var newValue = value as FixupCollection<AnnotationsSubtypes>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAnnotationsSubtypes;
                    }
                }
            }
        }
        private ICollection<AnnotationsSubtypes> _annotationsSubtypes;

        #endregion

        #region Association Fixup
    
        private void FixupAnnotations(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Annotations item in e.NewItems)
                {
                    item.AnnotationsStates = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Annotations item in e.OldItems)
                {
                    if (ReferenceEquals(item.AnnotationsStates, this))
                    {
                        item.AnnotationsStates = null;
                    }
                }
            }
        }
    
        private void FixupAnnotationsSubtypes(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (AnnotationsSubtypes item in e.NewItems)
                {
                    item.AnnotationsStates = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (AnnotationsSubtypes item in e.OldItems)
                {
                    if (ReferenceEquals(item.AnnotationsStates, this))
                    {
                        item.AnnotationsStates = null;
                    }
                }
            }
        }

        #endregion

    }
}
