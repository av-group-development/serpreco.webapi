//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace DAL.Entity
{
    public partial class MotivoBaja
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual int IdMotivoBaja
        {
            get;
            set;
        }
    
        public virtual string Idioma
        {
            get;
            set;
        }
    
        public virtual string Descripcion
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<Studio> Studio
        {
            get
            {
                if (_studio == null)
                {
                    var newCollection = new FixupCollection<Studio>();
                    newCollection.CollectionChanged += FixupStudio;
                    _studio = newCollection;
                }
                return _studio;
            }
            set
            {
                if (!ReferenceEquals(_studio, value))
                {
                    var previousValue = _studio as FixupCollection<Studio>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupStudio;
                    }
                    _studio = value;
                    var newValue = value as FixupCollection<Studio>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupStudio;
                    }
                }
            }
        }
        private ICollection<Studio> _studio;

        #endregion

        #region Association Fixup
    
        private void FixupStudio(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Studio item in e.NewItems)
                {
                    item.MotivoBaja = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Studio item in e.OldItems)
                {
                    if (ReferenceEquals(item.MotivoBaja, this))
                    {
                        item.MotivoBaja = null;
                    }
                }
            }
        }

        #endregion

    }
}
