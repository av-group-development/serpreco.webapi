﻿using System;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;

namespace DAL.Utils
{
    public static class Extensiones
    {
        #region LINQ

        public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            // build parameter map (from parameters of second to parameters of first)
            var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);

            // replace parameters in the second lambda expression with parameters from the first
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);

            // apply composition of lambda expression bodies to parameters from the first expression 
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.And);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.Or);
        }

        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.AndAlso);
        }

        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.OrElse);
        }

        #endregion

        #region Objetos

        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        public static bool CanDeleteEntity<T>(this T source)
        {                        
            // para cada propiedad del tipo que no tiene el espacio de nombres "System" y que no es RelationshipManager (?) y que sea colección                                      
            foreach (PropertyInfo pi in source.GetType().GetProperties().Where(p => p.PropertyType.Namespace != "System" &&
                                                                                    p.PropertyType.Name != "RelationshipManager" &&
                                                                                    p.PropertyType.GetInterface(typeof(IEnumerable<>).FullName) != null)) 
            {                                             
                // si la colección no es nula
                if (source.GetType().GetProperty(pi.Name).GetValue(source, null) != null)
                {                    
                    IEnumerable collectionValue = (IEnumerable)source.GetType().GetProperty(pi.Name).GetValue(source, null);
                    // recorremos la colección para ver si contiene elementos                    
                    foreach (object o in collectionValue)                    
                        return false;                    
                }            
            }

            return true;
        }

        #endregion

        public static dynamic SinHora(this DateTime? FechaYHora)
        {
            if (FechaYHora == null)
            {
                return null;
            }
            else
            {
                return ((DateTime)FechaYHora).Date;
            }
        }

        public static DateTime SinHora(this DateTime FechaYHora)
        {
            return ((DateTime)FechaYHora).Date;
        }

        public static StringContent AsJson(this object o)
             => new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
    }
}
