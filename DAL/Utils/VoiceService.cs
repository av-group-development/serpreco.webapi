﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DAL.Utils
{
    public class VoiceService
    {
        private static HttpClient client;

        public bool MakeCall(string ext, string numberphone,string apiKey,string baseUrl)
        {

            try
            {
                client = new HttpClient();
                
                var data = new { extension = ext, destination = numberphone };

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Authorization", $"VTCRM-Basic {apiKey}");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.PostAsync(baseUrl, data.AsJson());

                if (response != null && response.Id > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (System.Net.WebException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
