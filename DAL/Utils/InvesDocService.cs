﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.ServiceModel.Security;
using DAL.InvesdocWS;
using DAL.InvesDocCmis;
using DAL.ViewModel;

namespace Serpreco.DAL.Utils
{
    public class InvesDocService
    {
        public bool UploadStudioSingleFile(StudioInvesDocumentModelDAL document)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);
                var preparedFileName = PrepareFileName(document.Nombre);
                var preparedProps = preparedFileName.Split('-');

                InvesdocWSClient idocCli = new InvesdocWSClient();

                cmisContentStreamType content = new cmisContentStreamType();

                string expediente = $"{document.StudyId}-{document.ProposalId}";
                fieldVO[] props = PrepararCamposParaNuevaSubida(document.sPerson, "COR", document.DocumentType, DateTime.Now, document.Date, expediente);

                idocCli.ClientCredentials.UserName.UserName = "SYSSUPERUSER";
                idocCli.ClientCredentials.UserName.Password = "SYSPASSWORD";

                idocCli.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                
                inputDocumentVO inputDoc = new inputDocumentVO();
               
                content.mimeType = "application/pdf";
                inputDoc.data = document.File; //Convert.FromBase64String(document.File);
                inputDoc.name = preparedFileName;
                inputDoc.classifierId = "0";

                contentObjectVO newObject = idocCli.createContentObject("4", props, new inputDocumentVO[] { inputDoc });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string PrepareFileName(string filename)
        {
            byte[] decodedBytes = null;
            string decodedFileName = "";

            if (filename.Contains("?"))
            {
                decodedBytes = Convert.FromBase64String(extraerStringBase64(filename));
                decodedFileName = Encoding.UTF8.GetString(decodedBytes);
            }
            else
            {
                decodedFileName = filename;
            }

            decodedFileName = decodedFileName.Replace(" ", "_");

            return decodedFileName;
        }

        private string extraerStringBase64(string cadena)
        {
            var cadenaParseada = cadena.Split('?');

            return cadenaParseada[3];
        }

        private bool IgnoreCertificateErrorHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private fieldVO[] PrepararCamposParaNuevaSubida(string clientCode, string docType, DocumentTypeEnum SubType, DateTime loadDate, DateTime documentDate, string expediente)
        {
            string docSubType = string.Empty;

            switch (SubType)
            {
                case DocumentTypeEnum.Presupuesto:
                    docSubType = "CORPRE";
                    break;
                case DocumentTypeEnum.Cuestionario:
                    docSubType = "CORQSF";
                    break;
                case DocumentTypeEnum.Solicitud:
                    docSubType = "CORSAS";
                    break;
                case DocumentTypeEnum.Otros:
                    docSubType = "CORDOC";
                    break;
                default:
                    break;
            }

            string location = documentDate.ToString("yyyyMMdd");

            List<fieldVO> propsList = new List<fieldVO>
            {
                getFldVO(1, clientCode),
                getFldVO(2, docType),
                getFldVO(3, docSubType),
                getFldVO(4, loadDate),
                getFldVO(5, location),
                getFldVO(6, documentDate),
                getFldVO(7, expediente)
            };

            return propsList.ToArray();

        }

        private fieldVO getFldVO(int fieldId, object value)
        {
            fieldVO fld = new fieldVO();
            fld.fieldId = fieldId.ToString();
            fld.values = new object[] { value };
            return fld;
        }

        public List<StudioProposalInvesDocumentModel> getDocumentsStudy(int studyId, int proposalId, string sPerson)
        {
            List<StudioProposalInvesDocumentModel> result = new List<StudioProposalInvesDocumentModel>();

            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);
                InvesdocWSClient idocCli = new InvesdocWSClient();

                idocCli.ClientCredentials.UserName.UserName = "SYSSUPERUSER";
                idocCli.ClientCredentials.UserName.Password = "SYSPASSWORD";

                idocCli.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                string expediente = $"{studyId}-{proposalId}";

                fieldFilter filtro1 = new fieldFilter();
                filtro1.fieldId = "1";  // clientCode 
                filtro1.@operator = searchOperatorEnum.EQUALS;
                filtro1.operatorSpecified = true;
                filtro1.values = new object[] { sPerson };

                fieldFilter filtro2 = new fieldFilter();
                filtro2.fieldId = "7";  // expediente
                filtro2.@operator = searchOperatorEnum.EQUALS;
                filtro2.operatorSpecified = true;
                filtro2.values = new object[] { expediente };

                fieldFilter filtro3 = new fieldFilter();
                filtro3.fieldId = "2";  // docType
                filtro3.@operator = searchOperatorEnum.EQUALS;
                filtro3.operatorSpecified = true;
                filtro3.values = new object[] { "COR" };

                //fieldFilter[] filtros = new fieldFilter[] { filtro1, filtro2, filtro3 };

                fieldFilter[] filtros = new fieldFilter[] { filtro1,filtro2, filtro3 };

                sortField sort = new sortField();

                sort.fieldId = "1";
                sort.sortType = sortTypeEnum.ASCENDING;

                paginationContext pagination = new paginationContext();
                pagination.maxNumItems = 20;
                pagination.pageNumber = 1;
                pagination.pageSize = 100;
                pagination.skipCount = 0;

                contentObjectSearchCriteria contentObjectSearchCriteria = new contentObjectSearchCriteria
                {
                    contentTypeId = "4",
                    filters = filtros,
                    returnFieldIds = new string[] { "1", "2", "3", "4", "6", "7" },
                    paginationContext = pagination
                };

                paginatedList resultado = idocCli.searchContentObjects(contentObjectSearchCriteria);
                
                result = this.ConvertPaginatedListToModel(resultado,proposalId,idocCli);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<StudioProposalInvesDocumentModel> ConvertPaginatedListToModel(paginatedList listado,int proposalId, InvesdocWSClient idocCli)
        {
            try
            {
                List<StudioProposalInvesDocumentModel> result = new List<StudioProposalInvesDocumentModel>();

                foreach (var item in listado.contentObjects)
                {
                    contentObjectVO newObjectPrueba = idocCli.getContentObject(item.contentTypeId, item.contentObjectId, true);

                    foreach (var classifier in newObjectPrueba.classifiers)
                    {
                        foreach (var docs in classifier.documents)
                        {
                            DocumentTypeEnum? subType = getSubType(item.fields);

                            if (subType.HasValue)
                            {
                                StudioProposalInvesDocumentModel ent = new StudioProposalInvesDocumentModel();

                                ent.DocumentId = docs.documentId;
                                ent.Nombre = docs.name;
                                ent.DocumentBase64 = Convert.ToBase64String(docs.data);
                                ent.ProposalId = proposalId;
                                ent.DocumentType = subType.Value;
                                ent.ContentObjectId = item.contentObjectId;
                                ent.ContentTypeId = item.contentTypeId;
                                ent.DocumentData = getDocumentData(item.fields);

                                result.Add(ent);
                            }                            
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        private DateTime? getDocumentData(fieldVO[] propiedades)
        {
            foreach (var item in propiedades)
            {
                if (item.fieldId == "6")
                {
                    return DateTime.Parse(item.values[0].ToString());
                }
            }

            return null;
        }

        private DocumentTypeEnum? getSubType(fieldVO[] propiedades)
        {
            string subType = string.Empty;

            foreach (var item in propiedades)
            {
                if (item.fieldId == "3")
                {
                    subType = item.values[0].ToString();
                }
            }

            switch (subType)
            {
                case "CORPRE":
                    return DocumentTypeEnum.Presupuesto;
                case "CORQSF":
                    return DocumentTypeEnum.Cuestionario;
                case "CORSAS":
                    return DocumentTypeEnum.Solicitud;
                case "CORDOC":
                    return DocumentTypeEnum.Otros;
                default:
                    return null;
            }
        }
    }
}
