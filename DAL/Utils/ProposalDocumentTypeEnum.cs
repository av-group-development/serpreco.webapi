﻿public enum DocumentTypeEnum
{
    Presupuesto = 1,
    Cuestionario = 2,
    Solicitud = 3,
    Otros = 4
}
