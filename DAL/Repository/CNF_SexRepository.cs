
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_SexRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_SexRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_SexRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Sex> GetFilters(Expression<Func<CNF_Sex, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Sex.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Sex>GetIQueryFilters(Expression<Func<CNF_Sex, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Sex.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Sex)
                    return true;//(CNF_Sex)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Sex Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Sex)
                    return (CNF_Sex)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
