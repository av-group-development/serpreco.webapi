
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_CommercialAreaRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_CommercialAreaRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_CommercialAreaRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_CommercialArea> GetFilters(Expression<Func<CNF_CommercialArea, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_CommercialArea.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_CommercialArea>GetIQueryFilters(Expression<Func<CNF_CommercialArea, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_CommercialArea.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_CommercialArea)
                    return true;//(CNF_CommercialArea)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_CommercialArea Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_CommercialArea)
                    return (CNF_CommercialArea)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
