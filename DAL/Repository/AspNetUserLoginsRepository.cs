
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AspNetUserLoginsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AspNetUserLoginsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AspNetUserLoginsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AspNetUserLogins> GetFilters(Expression<Func<AspNetUserLogins, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetUserLogins.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AspNetUserLogins>GetIQueryFilters(Expression<Func<AspNetUserLogins, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetUserLogins.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AspNetUserLogins)
                    return true;//(AspNetUserLogins)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AspNetUserLogins Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AspNetUserLogins)
                    return (AspNetUserLogins)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
