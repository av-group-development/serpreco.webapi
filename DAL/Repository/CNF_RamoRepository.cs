
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_RamoRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_RamoRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_RamoRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Ramo> GetFilters(Expression<Func<CNF_Ramo, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Ramo.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Ramo>GetIQueryFilters(Expression<Func<CNF_Ramo, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Ramo.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Ramo)
                    return true;//(CNF_Ramo)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Ramo Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Ramo)
                    return (CNF_Ramo)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
