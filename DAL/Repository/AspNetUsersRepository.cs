
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AspNetUsersRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AspNetUsersRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AspNetUsersRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AspNetUsers> GetFilters(Expression<Func<AspNetUsers, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetUsers.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AspNetUsers>GetIQueryFilters(Expression<Func<AspNetUsers, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetUsers.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AspNetUsers)
                    return true;//(AspNetUsers)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AspNetUsers Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AspNetUsers)
                    return (AspNetUsers)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
