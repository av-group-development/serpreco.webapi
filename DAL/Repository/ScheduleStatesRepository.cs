
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ScheduleStatesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ScheduleStatesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ScheduleStatesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<ScheduleStates> GetFilters(Expression<Func<ScheduleStates, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleStates.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<ScheduleStates>GetIQueryFilters(Expression<Func<ScheduleStates, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleStates.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is ScheduleStates)
                    return true;//(ScheduleStates)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public ScheduleStates Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is ScheduleStates)
                    return (ScheduleStates)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
