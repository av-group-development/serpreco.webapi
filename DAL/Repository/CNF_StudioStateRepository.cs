
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_StudioStateRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_StudioStateRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_StudioStateRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_StudioState> GetFilters(Expression<Func<CNF_StudioState, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_StudioState.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_StudioState>GetIQueryFilters(Expression<Func<CNF_StudioState, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_StudioState.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_StudioState)
                    return true;//(CNF_StudioState)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_StudioState Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_StudioState)
                    return (CNF_StudioState)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
