
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AnnotationsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AnnotationsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AnnotationsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Annotations> GetFilters(Expression<Func<Annotations, Boolean>> filtro)
		{
			var query = _CommonEnt.Annotations.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Annotations>GetIQueryFilters(Expression<Func<Annotations, Boolean>> filtro)
		{
			var query = _CommonEnt.Annotations.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Annotations)
                    return true;//(Annotations)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Annotations Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Annotations)
                    return (Annotations)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
