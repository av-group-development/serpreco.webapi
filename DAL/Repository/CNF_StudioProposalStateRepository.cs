
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_StudioProposalStateRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_StudioProposalStateRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_StudioProposalStateRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_StudioProposalState> GetFilters(Expression<Func<CNF_StudioProposalState, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_StudioProposalState.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_StudioProposalState>GetIQueryFilters(Expression<Func<CNF_StudioProposalState, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_StudioProposalState.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_StudioProposalState)
                    return true;//(CNF_StudioProposalState)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_StudioProposalState Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_StudioProposalState)
                    return (CNF_StudioProposalState)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
