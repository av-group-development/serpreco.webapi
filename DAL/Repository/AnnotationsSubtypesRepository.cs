
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AnnotationsSubtypesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AnnotationsSubtypesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AnnotationsSubtypesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AnnotationsSubtypes> GetFilters(Expression<Func<AnnotationsSubtypes, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsSubtypes.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AnnotationsSubtypes>GetIQueryFilters(Expression<Func<AnnotationsSubtypes, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsSubtypes.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AnnotationsSubtypes)
                    return true;//(AnnotationsSubtypes)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AnnotationsSubtypes Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AnnotationsSubtypes)
                    return (AnnotationsSubtypes)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
