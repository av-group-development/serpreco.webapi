
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ScheduleTypesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ScheduleTypesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ScheduleTypesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<ScheduleTypes> GetFilters(Expression<Func<ScheduleTypes, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleTypes.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<ScheduleTypes>GetIQueryFilters(Expression<Func<ScheduleTypes, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleTypes.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is ScheduleTypes)
                    return true;//(ScheduleTypes)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public ScheduleTypes Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is ScheduleTypes)
                    return (ScheduleTypes)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
