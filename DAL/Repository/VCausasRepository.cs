
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VCausasRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VCausasRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VCausasRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VCausas> GetFilters(Expression<Func<VCausas, Boolean>> filtro)
		{
			var query = _CommonEnt.VCausas.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VCausas>GetIQueryFilters(Expression<Func<VCausas, Boolean>> filtro)
		{
			var query = _CommonEnt.VCausas.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VCausas)
                    return true;//(VCausas)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VCausas Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VCausas)
                    return (VCausas)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
