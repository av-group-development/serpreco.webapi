
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class MotivoBajaRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  MotivoBajaRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  MotivoBajaRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<MotivoBaja> GetFilters(Expression<Func<MotivoBaja, Boolean>> filtro)
		{
			var query = _CommonEnt.MotivoBaja.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<MotivoBaja>GetIQueryFilters(Expression<Func<MotivoBaja, Boolean>> filtro)
		{
			var query = _CommonEnt.MotivoBaja.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is MotivoBaja)
                    return true;//(MotivoBaja)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public MotivoBaja Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is MotivoBaja)
                    return (MotivoBaja)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
