
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VStudioProposalRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VStudioProposalRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VStudioProposalRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VStudioProposal> GetFilters(Expression<Func<VStudioProposal, Boolean>> filtro)
		{
			var query = _CommonEnt.VStudioProposal.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VStudioProposal>GetIQueryFilters(Expression<Func<VStudioProposal, Boolean>> filtro)
		{
			var query = _CommonEnt.VStudioProposal.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VStudioProposal)
                    return true;//(VStudioProposal)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VStudioProposal Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VStudioProposal)
                    return (VStudioProposal)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
 
// fin repositorios

