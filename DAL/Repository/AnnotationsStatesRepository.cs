
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AnnotationsStatesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AnnotationsStatesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AnnotationsStatesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AnnotationsStates> GetFilters(Expression<Func<AnnotationsStates, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsStates.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AnnotationsStates>GetIQueryFilters(Expression<Func<AnnotationsStates, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsStates.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AnnotationsStates)
                    return true;//(AnnotationsStates)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AnnotationsStates Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AnnotationsStates)
                    return (AnnotationsStates)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
