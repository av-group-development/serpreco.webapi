
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_RequestStateRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_RequestStateRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_RequestStateRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_RequestState> GetFilters(Expression<Func<CNF_RequestState, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_RequestState.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_RequestState>GetIQueryFilters(Expression<Func<CNF_RequestState, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_RequestState.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_RequestState)
                    return true;//(CNF_RequestState)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_RequestState Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_RequestState)
                    return (CNF_RequestState)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
