
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class SubAgentGroupRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  SubAgentGroupRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  SubAgentGroupRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<SubAgentGroup> GetFilters(Expression<Func<SubAgentGroup, Boolean>> filtro)
		{
			var query = _CommonEnt.SubAgentGroup.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<SubAgentGroup>GetIQueryFilters(Expression<Func<SubAgentGroup, Boolean>> filtro)
		{
			var query = _CommonEnt.SubAgentGroup.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is SubAgentGroup)
                    return true;//(SubAgentGroup)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public SubAgentGroup Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is SubAgentGroup)
                    return (SubAgentGroup)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
