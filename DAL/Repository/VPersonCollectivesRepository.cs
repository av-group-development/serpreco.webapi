
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VPersonCollectivesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VPersonCollectivesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VPersonCollectivesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VPersonCollectives> GetFilters(Expression<Func<VPersonCollectives, Boolean>> filtro)
		{
			var query = _CommonEnt.VPersonCollectives.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VPersonCollectives>GetIQueryFilters(Expression<Func<VPersonCollectives, Boolean>> filtro)
		{
			var query = _CommonEnt.VPersonCollectives.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VPersonCollectives)
                    return true;//(VPersonCollectives)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VPersonCollectives Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VPersonCollectives)
                    return (VPersonCollectives)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
