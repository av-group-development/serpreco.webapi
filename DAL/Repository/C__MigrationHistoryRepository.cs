
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class C__MigrationHistoryRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  C__MigrationHistoryRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  C__MigrationHistoryRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<C__MigrationHistory> GetFilters(Expression<Func<C__MigrationHistory, Boolean>> filtro)
		{
			var query = _CommonEnt.C__MigrationHistory.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<C__MigrationHistory>GetIQueryFilters(Expression<Func<C__MigrationHistory, Boolean>> filtro)
		{
			var query = _CommonEnt.C__MigrationHistory.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is C__MigrationHistory)
                    return true;//(C__MigrationHistory)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public C__MigrationHistory Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is C__MigrationHistory)
                    return (C__MigrationHistory)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
