
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class PrescriptorsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  PrescriptorsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  PrescriptorsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Prescriptors> GetFilters(Expression<Func<Prescriptors, Boolean>> filtro)
		{
			var query = _CommonEnt.Prescriptors.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Prescriptors>GetIQueryFilters(Expression<Func<Prescriptors, Boolean>> filtro)
		{
			var query = _CommonEnt.Prescriptors.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Prescriptors)
                    return true;//(Prescriptors)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Prescriptors Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Prescriptors)
                    return (Prescriptors)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
