
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_ProposalActionsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_ProposalActionsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_ProposalActionsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_ProposalActions> GetFilters(Expression<Func<CNF_ProposalActions, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_ProposalActions.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_ProposalActions>GetIQueryFilters(Expression<Func<CNF_ProposalActions, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_ProposalActions.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_ProposalActions)
                    return true;//(CNF_ProposalActions)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_ProposalActions Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_ProposalActions)
                    return (CNF_ProposalActions)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
