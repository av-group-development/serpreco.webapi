
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_Ramo_LanguageRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_Ramo_LanguageRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_Ramo_LanguageRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Ramo_Language> GetFilters(Expression<Func<CNF_Ramo_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Ramo_Language.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Ramo_Language>GetIQueryFilters(Expression<Func<CNF_Ramo_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Ramo_Language.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Ramo_Language)
                    return true;//(CNF_Ramo_Language)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Ramo_Language Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Ramo_Language)
                    return (CNF_Ramo_Language)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
