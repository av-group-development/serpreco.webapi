
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VStudioRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VStudioRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VStudioRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VStudio> GetFilters(Expression<Func<VStudio, Boolean>> filtro)
		{
			var query = _CommonEnt.VStudio.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VStudio>GetIQueryFilters(Expression<Func<VStudio, Boolean>> filtro)
		{
			var query = _CommonEnt.VStudio.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VStudio)
                    return true;//(VStudio)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VStudio Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VStudio)
                    return (VStudio)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
