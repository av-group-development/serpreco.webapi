
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class StudioProposalRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  StudioProposalRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  StudioProposalRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<StudioProposal> GetFilters(Expression<Func<StudioProposal, Boolean>> filtro)
		{
			var query = _CommonEnt.StudioProposal.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<StudioProposal>GetIQueryFilters(Expression<Func<StudioProposal, Boolean>> filtro)
		{
			var query = _CommonEnt.StudioProposal.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is StudioProposal)
                    return true;//(StudioProposal)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public StudioProposal Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is StudioProposal)
                    return (StudioProposal)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
