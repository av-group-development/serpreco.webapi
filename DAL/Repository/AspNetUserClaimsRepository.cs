
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AspNetUserClaimsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AspNetUserClaimsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AspNetUserClaimsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AspNetUserClaims> GetFilters(Expression<Func<AspNetUserClaims, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetUserClaims.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AspNetUserClaims>GetIQueryFilters(Expression<Func<AspNetUserClaims, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetUserClaims.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AspNetUserClaims)
                    return true;//(AspNetUserClaims)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AspNetUserClaims Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AspNetUserClaims)
                    return (AspNetUserClaims)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
