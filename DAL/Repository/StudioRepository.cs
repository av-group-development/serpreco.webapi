
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class StudioRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  StudioRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  StudioRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Studio> GetFilters(Expression<Func<Studio, Boolean>> filtro)
		{
			var query = _CommonEnt.Studio.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Studio>GetIQueryFilters(Expression<Func<Studio, Boolean>> filtro)
		{
			var query = _CommonEnt.Studio.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Studio)
                    return true;//(Studio)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Studio Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Studio)
                    return (Studio)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
