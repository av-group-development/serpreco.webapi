
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ScheduleSubTypesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ScheduleSubTypesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ScheduleSubTypesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<ScheduleSubTypes> GetFilters(Expression<Func<ScheduleSubTypes, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleSubTypes.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<ScheduleSubTypes>GetIQueryFilters(Expression<Func<ScheduleSubTypes, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleSubTypes.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is ScheduleSubTypes)
                    return true;//(ScheduleSubTypes)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public ScheduleSubTypes Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is ScheduleSubTypes)
                    return (ScheduleSubTypes)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
