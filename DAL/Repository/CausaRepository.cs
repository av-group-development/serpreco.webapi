
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CausaRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CausaRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CausaRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Causa> GetFilters(Expression<Func<Causa, Boolean>> filtro)
		{
			var query = _CommonEnt.Causa.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Causa>GetIQueryFilters(Expression<Func<Causa, Boolean>> filtro)
		{
			var query = _CommonEnt.Causa.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Causa)
                    return true;//(Causa)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Causa Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Causa)
                    return (Causa)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
