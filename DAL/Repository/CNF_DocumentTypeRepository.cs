
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_DocumentTypeRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_DocumentTypeRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_DocumentTypeRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_DocumentType> GetFilters(Expression<Func<CNF_DocumentType, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_DocumentType.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_DocumentType>GetIQueryFilters(Expression<Func<CNF_DocumentType, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_DocumentType.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_DocumentType)
                    return true;//(CNF_DocumentType)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_DocumentType Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_DocumentType)
                    return (CNF_DocumentType)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
