
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AssesorsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AssesorsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AssesorsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Assesors> GetFilters(Expression<Func<Assesors, Boolean>> filtro)
		{
			var query = _CommonEnt.Assesors.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Assesors>GetIQueryFilters(Expression<Func<Assesors, Boolean>> filtro)
		{
			var query = _CommonEnt.Assesors.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Assesors)
                    return true;//(Assesors)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Assesors Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Assesors)
                    return (Assesors)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
