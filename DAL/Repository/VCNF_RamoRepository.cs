
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VCNF_RamoRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VCNF_RamoRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VCNF_RamoRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VCNF_Ramo> GetFilters(Expression<Func<VCNF_Ramo, Boolean>> filtro)
		{
			var query = _CommonEnt.VCNF_Ramo.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VCNF_Ramo>GetIQueryFilters(Expression<Func<VCNF_Ramo, Boolean>> filtro)
		{
			var query = _CommonEnt.VCNF_Ramo.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VCNF_Ramo)
                    return true;//(VCNF_Ramo)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VCNF_Ramo Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VCNF_Ramo)
                    return (VCNF_Ramo)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
