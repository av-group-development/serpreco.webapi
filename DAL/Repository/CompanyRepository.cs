
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CompanyRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CompanyRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CompanyRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Company> GetFilters(Expression<Func<Company, Boolean>> filtro)
		{
			var query = _CommonEnt.Company.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Company>GetIQueryFilters(Expression<Func<Company, Boolean>> filtro)
		{
			var query = _CommonEnt.Company.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Company)
                    return true;//(Company)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Company Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Company)
                    return (Company)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
