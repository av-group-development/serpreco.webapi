
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class PersonRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  PersonRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  PersonRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Person> GetFilters(Expression<Func<Person, Boolean>> filtro)
		{
			var query = _CommonEnt.Person.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Person>GetIQueryFilters(Expression<Func<Person, Boolean>> filtro)
		{
			var query = _CommonEnt.Person.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Person)
                    return true;//(Person)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Person Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Person)
                    return (Person)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
