
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_PeriodicityRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_PeriodicityRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_PeriodicityRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Periodicity> GetFilters(Expression<Func<CNF_Periodicity, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Periodicity.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Periodicity>GetIQueryFilters(Expression<Func<CNF_Periodicity, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Periodicity.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Periodicity)
                    return true;//(CNF_Periodicity)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Periodicity Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Periodicity)
                    return (CNF_Periodicity)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
