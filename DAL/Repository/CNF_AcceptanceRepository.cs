
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_AcceptanceRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_AcceptanceRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_AcceptanceRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Acceptance> GetFilters(Expression<Func<CNF_Acceptance, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Acceptance.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Acceptance>GetIQueryFilters(Expression<Func<CNF_Acceptance, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Acceptance.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Acceptance)
                    return true;//(CNF_Acceptance)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Acceptance Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Acceptance)
                    return (CNF_Acceptance)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
