
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_LegalFormRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_LegalFormRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_LegalFormRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_LegalForm> GetFilters(Expression<Func<CNF_LegalForm, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_LegalForm.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_LegalForm>GetIQueryFilters(Expression<Func<CNF_LegalForm, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_LegalForm.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_LegalForm)
                    return true;//(CNF_LegalForm)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_LegalForm Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_LegalForm)
                    return (CNF_LegalForm)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
