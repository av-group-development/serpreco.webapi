
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_PostalCode_CityRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_PostalCode_CityRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_PostalCode_CityRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_PostalCode_City> GetFilters(Expression<Func<CNF_PostalCode_City, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_PostalCode_City.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_PostalCode_City>GetIQueryFilters(Expression<Func<CNF_PostalCode_City, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_PostalCode_City.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_PostalCode_City)
                    return true;//(CNF_PostalCode_City)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_PostalCode_City Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_PostalCode_City)
                    return (CNF_PostalCode_City)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
