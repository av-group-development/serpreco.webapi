
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class NotificationRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  NotificationRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  NotificationRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Notification> GetFilters(Expression<Func<Notification, Boolean>> filtro)
		{
			var query = _CommonEnt.Notification.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Notification>GetIQueryFilters(Expression<Func<Notification, Boolean>> filtro)
		{
			var query = _CommonEnt.Notification.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Notification)
                    return true;//(Notification)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Notification Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Notification)
                    return (Notification)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
