
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_TreatmentRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_TreatmentRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_TreatmentRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Treatment> GetFilters(Expression<Func<CNF_Treatment, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Treatment.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Treatment>GetIQueryFilters(Expression<Func<CNF_Treatment, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Treatment.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Treatment)
                    return true;//(CNF_Treatment)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Treatment Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Treatment)
                    return (CNF_Treatment)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
