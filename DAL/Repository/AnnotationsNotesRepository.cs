
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AnnotationsNotesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AnnotationsNotesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AnnotationsNotesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AnnotationsNotes> GetFilters(Expression<Func<AnnotationsNotes, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsNotes.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AnnotationsNotes>GetIQueryFilters(Expression<Func<AnnotationsNotes, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsNotes.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AnnotationsNotes)
                    return true;//(AnnotationsNotes)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AnnotationsNotes Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AnnotationsNotes)
                    return (AnnotationsNotes)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
