
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_ProposalDocumentTypeRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_ProposalDocumentTypeRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_ProposalDocumentTypeRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_ProposalDocumentType> GetFilters(Expression<Func<CNF_ProposalDocumentType, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_ProposalDocumentType.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_ProposalDocumentType>GetIQueryFilters(Expression<Func<CNF_ProposalDocumentType, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_ProposalDocumentType.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_ProposalDocumentType)
                    return true;//(CNF_ProposalDocumentType)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_ProposalDocumentType Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_ProposalDocumentType)
                    return (CNF_ProposalDocumentType)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
