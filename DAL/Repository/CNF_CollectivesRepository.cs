
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_CollectivesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_CollectivesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_CollectivesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Collectives> GetFilters(Expression<Func<CNF_Collectives, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Collectives.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Collectives>GetIQueryFilters(Expression<Func<CNF_Collectives, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Collectives.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Collectives)
                    return true;//(CNF_Collectives)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Collectives Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Collectives)
                    return (CNF_Collectives)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
