
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ScheduleEntitiesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ScheduleEntitiesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ScheduleEntitiesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<ScheduleEntities> GetFilters(Expression<Func<ScheduleEntities, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleEntities.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<ScheduleEntities>GetIQueryFilters(Expression<Func<ScheduleEntities, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleEntities.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is ScheduleEntities)
                    return true;//(ScheduleEntities)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public ScheduleEntities Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is ScheduleEntities)
                    return (ScheduleEntities)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
