
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ScheduleRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ScheduleRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ScheduleRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Schedule> GetFilters(Expression<Func<Schedule, Boolean>> filtro)
		{
			var query = _CommonEnt.Schedule.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Schedule>GetIQueryFilters(Expression<Func<Schedule, Boolean>> filtro)
		{
			var query = _CommonEnt.Schedule.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Schedule)
                    return true;//(Schedule)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Schedule Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Schedule)
                    return (Schedule)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
