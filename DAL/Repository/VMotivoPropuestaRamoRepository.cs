
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VMotivoPropuestaRamoRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VMotivoPropuestaRamoRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VMotivoPropuestaRamoRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VMotivoPropuestaRamo> GetFilters(Expression<Func<VMotivoPropuestaRamo, Boolean>> filtro)
		{
			var query = _CommonEnt.VMotivoPropuestaRamo.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VMotivoPropuestaRamo>GetIQueryFilters(Expression<Func<VMotivoPropuestaRamo, Boolean>> filtro)
		{
			var query = _CommonEnt.VMotivoPropuestaRamo.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VMotivoPropuestaRamo)
                    return true;//(VMotivoPropuestaRamo)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VMotivoPropuestaRamo Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VMotivoPropuestaRamo)
                    return (VMotivoPropuestaRamo)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
