
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class sysdiagramsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  sysdiagramsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  sysdiagramsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<sysdiagrams> GetFilters(Expression<Func<sysdiagrams, Boolean>> filtro)
		{
			var query = _CommonEnt.sysdiagrams.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<sysdiagrams>GetIQueryFilters(Expression<Func<sysdiagrams, Boolean>> filtro)
		{
			var query = _CommonEnt.sysdiagrams.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is sysdiagrams)
                    return true;//(sysdiagrams)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public sysdiagrams Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is sysdiagrams)
                    return (sysdiagrams)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
