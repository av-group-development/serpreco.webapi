
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VCNF_PeriodicityRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VCNF_PeriodicityRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VCNF_PeriodicityRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VCNF_Periodicity> GetFilters(Expression<Func<VCNF_Periodicity, Boolean>> filtro)
		{
			var query = _CommonEnt.VCNF_Periodicity.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VCNF_Periodicity>GetIQueryFilters(Expression<Func<VCNF_Periodicity, Boolean>> filtro)
		{
			var query = _CommonEnt.VCNF_Periodicity.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VCNF_Periodicity)
                    return true;//(VCNF_Periodicity)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VCNF_Periodicity Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VCNF_Periodicity)
                    return (VCNF_Periodicity)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
