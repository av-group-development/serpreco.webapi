
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VGenerarInformeSolicitud_esRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VGenerarInformeSolicitud_esRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VGenerarInformeSolicitud_esRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VGenerarInformeSolicitud_es> GetFilters(Expression<Func<VGenerarInformeSolicitud_es, Boolean>> filtro)
		{
			var query = _CommonEnt.VGenerarInformeSolicitud_es.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VGenerarInformeSolicitud_es>GetIQueryFilters(Expression<Func<VGenerarInformeSolicitud_es, Boolean>> filtro)
		{
			var query = _CommonEnt.VGenerarInformeSolicitud_es.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VGenerarInformeSolicitud_es)
                    return true;//(VGenerarInformeSolicitud_es)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VGenerarInformeSolicitud_es Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VGenerarInformeSolicitud_es)
                    return (VGenerarInformeSolicitud_es)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
