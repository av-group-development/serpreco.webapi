
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ScheduleNotesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ScheduleNotesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ScheduleNotesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<ScheduleNotes> GetFilters(Expression<Func<ScheduleNotes, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleNotes.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<ScheduleNotes>GetIQueryFilters(Expression<Func<ScheduleNotes, Boolean>> filtro)
		{
			var query = _CommonEnt.ScheduleNotes.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is ScheduleNotes)
                    return true;//(ScheduleNotes)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public ScheduleNotes Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is ScheduleNotes)
                    return (ScheduleNotes)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
