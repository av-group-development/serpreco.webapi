
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_Sex_LanguageRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_Sex_LanguageRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_Sex_LanguageRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Sex_Language> GetFilters(Expression<Func<CNF_Sex_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Sex_Language.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Sex_Language>GetIQueryFilters(Expression<Func<CNF_Sex_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Sex_Language.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Sex_Language)
                    return true;//(CNF_Sex_Language)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Sex_Language Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Sex_Language)
                    return (CNF_Sex_Language)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
