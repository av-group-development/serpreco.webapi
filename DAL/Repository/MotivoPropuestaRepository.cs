
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class MotivoPropuestaRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  MotivoPropuestaRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  MotivoPropuestaRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<MotivoPropuesta> GetFilters(Expression<Func<MotivoPropuesta, Boolean>> filtro)
		{
			var query = _CommonEnt.MotivoPropuesta.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<MotivoPropuesta>GetIQueryFilters(Expression<Func<MotivoPropuesta, Boolean>> filtro)
		{
			var query = _CommonEnt.MotivoPropuesta.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is MotivoPropuesta)
                    return true;//(MotivoPropuesta)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public MotivoPropuesta Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is MotivoPropuesta)
                    return (MotivoPropuesta)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
