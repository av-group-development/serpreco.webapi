
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_DemarcacionRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_DemarcacionRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_DemarcacionRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Demarcacion> GetFilters(Expression<Func<CNF_Demarcacion, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Demarcacion.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Demarcacion>GetIQueryFilters(Expression<Func<CNF_Demarcacion, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Demarcacion.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Demarcacion)
                    return true;//(CNF_Demarcacion)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Demarcacion Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Demarcacion)
                    return (CNF_Demarcacion)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
