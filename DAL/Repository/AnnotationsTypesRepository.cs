
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AnnotationsTypesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AnnotationsTypesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AnnotationsTypesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AnnotationsTypes> GetFilters(Expression<Func<AnnotationsTypes, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsTypes.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AnnotationsTypes>GetIQueryFilters(Expression<Func<AnnotationsTypes, Boolean>> filtro)
		{
			var query = _CommonEnt.AnnotationsTypes.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AnnotationsTypes)
                    return true;//(AnnotationsTypes)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AnnotationsTypes Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AnnotationsTypes)
                    return (AnnotationsTypes)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
