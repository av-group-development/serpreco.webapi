
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_ChargeModeRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_ChargeModeRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_ChargeModeRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_ChargeMode> GetFilters(Expression<Func<CNF_ChargeMode, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_ChargeMode.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_ChargeMode>GetIQueryFilters(Expression<Func<CNF_ChargeMode, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_ChargeMode.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_ChargeMode)
                    return true;//(CNF_ChargeMode)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_ChargeMode Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_ChargeMode)
                    return (CNF_ChargeMode)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
