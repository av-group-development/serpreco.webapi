
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class AspNetRolesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  AspNetRolesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  AspNetRolesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<AspNetRoles> GetFilters(Expression<Func<AspNetRoles, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetRoles.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<AspNetRoles>GetIQueryFilters(Expression<Func<AspNetRoles, Boolean>> filtro)
		{
			var query = _CommonEnt.AspNetRoles.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is AspNetRoles)
                    return true;//(AspNetRoles)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public AspNetRoles Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is AspNetRoles)
                    return (AspNetRoles)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
