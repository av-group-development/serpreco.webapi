
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_TypesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_TypesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_TypesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Types> GetFilters(Expression<Func<CNF_Types, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Types.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Types>GetIQueryFilters(Expression<Func<CNF_Types, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Types.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Types)
                    return true;//(CNF_Types)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Types Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Types)
                    return (CNF_Types)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
