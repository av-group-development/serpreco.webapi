
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_CountryRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_CountryRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_CountryRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Country> GetFilters(Expression<Func<CNF_Country, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Country.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Country>GetIQueryFilters(Expression<Func<CNF_Country, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Country.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Country)
                    return true;//(CNF_Country)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Country Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Country)
                    return (CNF_Country)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
