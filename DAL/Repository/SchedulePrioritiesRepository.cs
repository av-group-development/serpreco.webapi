
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class SchedulePrioritiesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  SchedulePrioritiesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  SchedulePrioritiesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<SchedulePriorities> GetFilters(Expression<Func<SchedulePriorities, Boolean>> filtro)
		{
			var query = _CommonEnt.SchedulePriorities.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<SchedulePriorities>GetIQueryFilters(Expression<Func<SchedulePriorities, Boolean>> filtro)
		{
			var query = _CommonEnt.SchedulePriorities.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is SchedulePriorities)
                    return true;//(SchedulePriorities)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public SchedulePriorities Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is SchedulePriorities)
                    return (SchedulePriorities)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
