
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class MotivoPropuesta_LanguageRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  MotivoPropuesta_LanguageRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  MotivoPropuesta_LanguageRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<MotivoPropuesta_Language> GetFilters(Expression<Func<MotivoPropuesta_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.MotivoPropuesta_Language.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<MotivoPropuesta_Language>GetIQueryFilters(Expression<Func<MotivoPropuesta_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.MotivoPropuesta_Language.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is MotivoPropuesta_Language)
                    return true;//(MotivoPropuesta_Language)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public MotivoPropuesta_Language Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is MotivoPropuesta_Language)
                    return (MotivoPropuesta_Language)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
