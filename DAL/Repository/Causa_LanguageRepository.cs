
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class Causa_LanguageRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  Causa_LanguageRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  Causa_LanguageRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Causa_Language> GetFilters(Expression<Func<Causa_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.Causa_Language.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Causa_Language>GetIQueryFilters(Expression<Func<Causa_Language, Boolean>> filtro)
		{
			var query = _CommonEnt.Causa_Language.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Causa_Language)
                    return true;//(Causa_Language)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Causa_Language Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Causa_Language)
                    return (Causa_Language)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
