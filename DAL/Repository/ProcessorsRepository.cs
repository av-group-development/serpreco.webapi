
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class ProcessorsRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  ProcessorsRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  ProcessorsRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<Processors> GetFilters(Expression<Func<Processors, Boolean>> filtro)
		{
			var query = _CommonEnt.Processors.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<Processors>GetIQueryFilters(Expression<Func<Processors, Boolean>> filtro)
		{
			var query = _CommonEnt.Processors.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is Processors)
                    return true;//(Processors)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public Processors Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is Processors)
                    return (Processors)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
