
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class CNF_ProvinceRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  CNF_ProvinceRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  CNF_ProvinceRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<CNF_Province> GetFilters(Expression<Func<CNF_Province, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Province.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<CNF_Province>GetIQueryFilters(Expression<Func<CNF_Province, Boolean>> filtro)
		{
			var query = _CommonEnt.CNF_Province.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is CNF_Province)
                    return true;//(CNF_Province)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public CNF_Province Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is CNF_Province)
                    return (CNF_Province)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
