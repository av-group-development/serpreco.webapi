
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class PersonCollectivesRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  PersonCollectivesRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  PersonCollectivesRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<PersonCollectives> GetFilters(Expression<Func<PersonCollectives, Boolean>> filtro)
		{
			var query = _CommonEnt.PersonCollectives.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<PersonCollectives>GetIQueryFilters(Expression<Func<PersonCollectives, Boolean>> filtro)
		{
			var query = _CommonEnt.PersonCollectives.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is PersonCollectives)
                    return true;//(PersonCollectives)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public PersonCollectives Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is PersonCollectives)
                    return (PersonCollectives)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
