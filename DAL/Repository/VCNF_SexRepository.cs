
using System;
using System.Linq;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entity;

namespace DAL.Repository
{
	public partial class VCNF_SexRepository
	{
		Entities _CommonEnt;

		public Entities CurrentContext    
		{
			get
			{
				return _CommonEnt;
			}
		}

	   public  VCNF_SexRepository()
	   {
		_CommonEnt = new Entities();
		_CommonEnt.ContextOptions.ProxyCreationEnabled = false;
		_CommonEnt.ContextOptions.LazyLoadingEnabled = false;
	   }
   
	   public  VCNF_SexRepository(Entities context)
	   {
		_CommonEnt = context;
	   }
  
		public List<VCNF_Sex> GetFilters(Expression<Func<VCNF_Sex, Boolean>> filtro)
		{
			var query = _CommonEnt.VCNF_Sex.Where(filtro);
			return query.ToList();
		}
	
		public IQueryable<VCNF_Sex>GetIQueryFilters(Expression<Func<VCNF_Sex, Boolean>> filtro)
		{
			var query = _CommonEnt.VCNF_Sex.Where(filtro);
			return query;
		}

		public bool TryParse(object o)
        {
            if (o == null)
                return false;

            try
            {
                if (o is VCNF_Sex)
                    return true;//(VCNF_Sex)o;
            }
            catch
            {
                return false;
            }
            return false;

        }

		
		public VCNF_Sex Parse(object o)
        {
            if (o == null)
                return null;

            try
            {
                if (o is VCNF_Sex)
                    return (VCNF_Sex)o;
            }
            catch
            {
                return null;
            }
            return null;

        }
	}
}
