﻿namespace DAL.Enums
{
    public enum ModalMailEnum
    {
        SendQuestionarie = 1,
        SendBudget = 2,
        GenerateRequest = 3,
        SendAllQuestionaries = 4,
        SendAllBudgets = 5
    }
}
