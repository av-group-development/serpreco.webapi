﻿namespace DAL.Enums
{
    public enum StateEnum
    {
        Active = 1,
        Cancelled = 2,
        Contracted = 3,
        SentAdministration = 4,
        CancelledAdministration = 5,
        AcceptedAdministration = 6
    }
}