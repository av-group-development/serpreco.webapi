﻿namespace DAL.Enums
{
    public enum CommercialAreaEnum
    {
        AreaEmpresa = 1,
        AreaParticulars = 2
    }
}