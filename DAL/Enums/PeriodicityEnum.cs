﻿namespace DAL.Enums
{
    public enum PeriodicityEnum
    {
        Annual = 1,
        Biannual = 2,
        Quarterly = 3,
        Quadrimestral = 4,
        Monthly = 5,
        Weekly = 6,
        Daily = 7
    }
}