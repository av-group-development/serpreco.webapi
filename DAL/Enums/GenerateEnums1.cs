﻿ 

namespace DAL.Enums
{
	public enum ScheduleStatesEnum
	{ 
		pendiente = 1,
		hecho = 2,
		cancelado = 3,
	}

	public enum AnnotationsStatesEnum
	{ 
		pendiente = 1,
		hecho = 2,
		cancelado = 3,
	}
	
	public enum AnnotationsTypesEnum
	{ 
		pago = 1,
		reserva = 2,
		cambioEstado = 3,
		seguimiento = 4,
	}

	public enum AnnotationsSubtypesEnum
	{ 
		anulado = 1,
		abierto = 2,
		cerrado = 3,
	}

	public enum ScheduleTypesEnum
	{ 
		Seguiment = 1,
	}

	public enum ScheduleEntitiesEnum
	{ 
		Usuari = 1,
		Estudi = 2,
	}

	
	public enum ScheduleNotesEnum
	{ 
		Pendentderebrerespostadelqüestionari = 2,
		Pendentderebrerespostadelasolicitut = 3,
		Pendentderebrerespostadelpressupost = 4,
		Pendentderebrerespostaperenviamentdetotselsqüestionaris = 5,
		Pendentderebrerespostaperenviamentdetotselspressupostos = 6,
	}



	public enum SchedulePrioritiesEnum
	{ 
		Alta = 1,
		Mitja = 2,
		Baixa = 3,
	}
}
