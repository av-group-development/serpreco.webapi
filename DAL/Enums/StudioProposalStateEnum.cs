﻿namespace DAL.Enums
{
    public enum StudioProposalStateEnum
    {
        Pending = 1,
        Sent = 2,
        Accepted = 3,
        Cancelled = 4,
        Rejected = 5
    }
}