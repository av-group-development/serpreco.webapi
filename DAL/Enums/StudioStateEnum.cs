﻿namespace DAL.Enums
{
    public enum StudioStateEnum
    {
        Pending = 1,
        Sent = 2,
        Accepted = 3,
        Cancelled = 4,
        Rejected = 5,
        TimedOut = 6
    }
}