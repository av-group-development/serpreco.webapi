﻿
using DAL.Enums;
using System.Collections.Generic;

namespace DAL.ViewModel
{
   public class MailAttachTask
    {
        public  string to { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string idstudio { get; set; }
        public string from { get; set; }
        public List<Attach> files { get; set; }
        public ModalMailEnum option { get; set; }
        public string userLogged { get; set; }
    }

    public class Attach
    {
        public string DocumentBase64 { get; set; }
        public string Nombre { get; set; }
    }
}