﻿
namespace DAL.ViewModel
{
    /// <summary>
    /// Structure used to work with autocompletes
    /// </summary>
    public class AutoCompleteModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}