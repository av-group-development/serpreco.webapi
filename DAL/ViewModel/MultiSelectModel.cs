﻿
namespace DAL.ViewModel
{
    /// <summary>
    /// Structure used to work with multiselects
    /// </summary>
    public class MultiSelectModel
    {
        public int value { get; set; }
        public string label { get; set; }
    }
}