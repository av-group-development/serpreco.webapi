﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.ViewModel
{


        public class StudioInvesDocumentModelDAL
        {
            public string Nombre { get; set; }

            public DocumentTypeEnum DocumentType { get; set; }

            public int StudyId { get; set; }

            public int ProposalId { get; set; }

            public byte[] File { get; set; }

            public string sPerson { get; set; }

            public DateTime Date { get; set; }

        }

        public class StudioProposalInvesDocumentModel
        {
            public string Nombre { get; set; }

            public DocumentTypeEnum DocumentType { get; set; }

            public int ProposalId { get; set; }

            public string DocumentBase64 { get; set; }

            public string DocumentId { get; set; }

            public string ContentTypeId { get; set; }

            public string ContentObjectId { get; set; }

            public DateTime? DocumentData { get; set; }

            public string NombreCompleto
            {
                get
                {
                    if (this.DocumentData.HasValue)
                    {
                        return $"{Nombre} ({this.DocumentData.Value.ToString("dd/MM/yyyy")})";
                    }
                    else
                    {
                        return this.Nombre;
                    }
                }
            }

        }

        public class StudioInvesDocumentResponse
        {
            public List<InvesDocResponse> ficheros { get; set; }
            public List<StudioProposalInvesDocumentModel> documentos { get; set; }
        }

        public class InvesDocResponse
        {
            public string Name { get; set; }
            public bool Subido { get; set; }
        }
    }
