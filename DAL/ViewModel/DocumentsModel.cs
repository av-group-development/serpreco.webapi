﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class DocumentModel
    {
        public string Nombre { get; set; }
        public string DocumentBase64 { get; set; }
        public string Extension { get; set; }
    }
}
