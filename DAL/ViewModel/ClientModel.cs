﻿using System.Collections.Generic;

namespace DAL.ViewModel
{
    public class FullClient
    {
        public DAL.Entity.Person Client { get; set; }
        public List<MultiSelectModel> ClientCollectives { get; set; }
    }
}