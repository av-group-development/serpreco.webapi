﻿using DAL.Entity;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class FullStudio
    {
        public Studio Studio { get; set; }
        public List<StudioProposal> Proposals { get; set; }
        public Person Client { get; set; }
        public List<VPersonCollectives> ClientCollectives { get; set; }
    }

    public class StudioInvesDocumentModel
    {
        public DocumentTypeEnum DocumentType { get; set; }
        public int StudyId { get; set; }
        public int ProposalId { get; set; }
        public List<Attach> Files { get; set; }
        public string sPerson { get; set; }
        public DateTime Date { get; set; }
    }
}
