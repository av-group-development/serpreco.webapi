﻿namespace DAL.Partial
{
    public partial class Annotations
    {
        public virtual string IdAnnotationTypesDesc { get; set; }
        public virtual string IdAnnotationSubtypesDesc { get; set; }
        public virtual string IdAnnotationsStatesDesc { get; set; }
        public virtual string IdUserCreatedDesc { get; set; }
        public virtual string IdAnnotationTypesTranslate{ get; set; }
        public virtual string IdAnnotationSubtypesTranslate { get; set; }
        public virtual string IdAnnotationsStatesTranslate { get; set; }
        public virtual string IdPersonDesc { get; set; }

    }
}