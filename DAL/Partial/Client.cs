﻿using System;

namespace DAL.Partial
{
    public class ClientView
    {
        public int Id { get; set; }
        public string Sperson { get; set; }
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public int IdDocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime Birthday { get; set; }
        public string Notes { get; set; }
        public bool IsPreClient { get; set; }
        public int IdLegalForm { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdate { get; set; }
        public int BussinesAdvisor { get; set; }
        public int IdChargeMode { get; set; }
        public int IdSex { get; set; }
        public int IdLanguage { get; set; }
        public int IdTreatment { get; set; }
    }
}