﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Partial
{
    public partial class Schedule
    {
        public virtual string IdScheduleStatesDesc { get; set; }
        public virtual string IdScheduleTypesDesc { get; set; }
        public virtual string IdUserCreatedDesc { get; set; }
        public virtual string IdUserAssignedDesc { get; set; }
        public virtual string IdSchedulePrioritiesDesc { get; set; }
        public virtual string IdScheduleStatesTranslate { get; set; }
        public virtual string IdScheduleTypesTranslate { get; set; }
        public virtual string IdSchedulePrioritiesTranslate { get; set; }
        public virtual string IdPersonDesc { get; set; }
        public virtual int DayPlan { get; set; }
        public virtual int MonthPlan { get; set; }
        public virtual string TimePlan { get; set; }

    }
}