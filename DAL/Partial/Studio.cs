﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Partial
{
    public partial class FullStudioView
    {
        public StudioView StudioView { get; set; }
        public IEnumerable<StudioProposalView> StudioProposalView { get; set; }
        public ClientView ClientView { get; set; }
        public IEnumerable<VPersonCollectivesView> VPersonCollectivesView { get; set; }
    }

    public partial class StudioView
    {
        public int Id { get; set; }
        public int RamoId { get; set; }
        public string Notes { get; set; }
        public string IBAN { get; set; }
        public DateTime EffectDate { get; set; }
        public int ReasonId { get; set; }
        public int ProcessorId { get; set; }
        public int CommercialAreaId { get; set; }
        public int SubAgentId { get; set; }
        public int PrescriberId { get; set; }
        public int StateId { get; set; }
    }

    public partial class StudioProposalView
    {
        public int StudioId { get; set; }
        public int ProposalId { get; set; }
        public int Id { get; set; }
        public int Company { get; set; }
        public string Notes { get; set; }
        public int StateId { get; set; }
        public int TotalBonus { get; set; }
        public int PeriodicityId { get; set; }
        public string Reason { get; set; }
        public string Proposal { get; set; }
    }

    public partial class VPersonCollectivesView
    {
        public int Id { get; set; }
        public int IdPerson { get; set; }
        public int IdCollective { get; set; }
    }
}