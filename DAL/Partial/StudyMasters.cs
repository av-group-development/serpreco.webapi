﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Partial
{
    public class StudyMasters
    {
        public List<Assesors> assesors { get; set; }
        public List<Company> companies { get; set; }
        public List<SubAgentGroup> subAgents { get; set; }
        public List<Processors> processors { get; set; }
        public List<Prescriptors> prescriptors { get; set; }
        public List<VCausas> causas { get; set; }
        public List<VMotivoPropuestaRamo> motivosPropuestas { get; set; }
        public List<CNF_Collectives> collectives { get; set; }
        public List<VCNF_Sex> sexes { get; set; }
        public List<VCNF_Ramo> ramos { get; set; }
        public List<VCNF_Periodicity> periodicities { get; set; }
        public List<MotivoBaja> motivosBaja { get; set; }
    }
}
