﻿using API.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API.Commons
{
    public static class DocumentCommon
    {
        public static List<DocumentModel> GetFilesFromPath(string path)
        {
            List<DocumentModel> ListResult = new List<DocumentModel>();

            bool existeDirectorio = Directory.Exists(path);

            if (existeDirectorio)
            {
                string[] files = Directory.GetFiles(path);

                foreach (var item in files)
                {
                    //Obtenemos los nombres de los ficheros, ahora se tiene que obtener su array de bytes
                    DocumentModel ent = new DocumentModel();
                    ent.Nombre = Path.GetFileName(item);
                    ent.DocumentBase64 = Convert.ToBase64String(File.ReadAllBytes(item));
                    ent.Extension = Path.GetExtension(item);
                    ListResult.Add(ent);
                }
            }

            return ListResult;
        }

    }
}
