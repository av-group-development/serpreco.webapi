﻿
using Microsoft.AspNetCore.Identity;

namespace API.Models.Entities
{
    public class AppUser : IdentityUser
    {
        // Extended Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
