﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        [Route("GetValues1")]
        [AllowAnonymous]
        [HttpGet]
        public string Get()
        {
            return  "GetValues1";
        }

        [Route("GetValues2")]
        [Authorize(Policy = "ApiUser")]
        [HttpGet]
        public string Get2()
        {
            return "GetValues2";
        }

        [Route("GetValues3")]
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public string Get3()
        {
            return "GetValues3";
        }

        [Route("GetValues4")]
        [Authorize(Roles = "Commercial")]
        [HttpGet]
        public string Get4()
        {
            return "GetValues4";
        }

        [Route("GetValues5")]
        [Authorize(Roles = "Vendor")]
        [HttpGet]
        public string Get5()
        {
            return "GetValues5";
        }
    }
}
