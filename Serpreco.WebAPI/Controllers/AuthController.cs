﻿using System.Security.Claims;
using System.Threading.Tasks;
using API.Auth;
using API.Helpers;
using API.Models;
using API.Models.Entities;
using API.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using DAL.Utils;

namespace API.Controllers
{
  
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;

        public AuthController(UserManager<AppUser> userManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;
        }


        [Route("login")]// POST api/auth/login
        [AllowAnonymous]
        [HttpPost]
     //   public async IHttpActionResult SignIn([FromBody] Login model)
        public async Task<IActionResult> login([FromBody] Login model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var identity = await GetClaimsIdentity(model.userName, model.pass);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }
            var user = await _userManager.FindByNameAsync(model.userName);
            var roles = await _userManager.GetRolesAsync(user);

            var jwt = await Tokens.GenerateJwt(roles, identity, _jwtFactory, model.userName, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
            return new OkObjectResult(jwt);
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
              
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }


        // POST api/auth/login
        [HttpPost("AddNewUser")]
        public async Task<IActionResult> AddNewUser([FromBody]CredentialsViewModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            AppUser user = new AppUser();
            user.UserName = credentials.UserName;
            user.PasswordHash = credentials.Password;

            var userNew = await _userManager.CreateAsync(user);

            return new OkObjectResult(user);
        }
    }
}
