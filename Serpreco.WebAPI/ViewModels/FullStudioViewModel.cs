﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class FullStudioViewModel
    {
        public StudioViewModel StudioView { get; set; }
        public List<StudioProposalViewModel> StudioProposalView { get; set; }
        public ClientViewModel ClientView { get; set; }
        public List<VPersonCollectivesViewModel> VPersonCollectivesView { get; set; }

    }
}
