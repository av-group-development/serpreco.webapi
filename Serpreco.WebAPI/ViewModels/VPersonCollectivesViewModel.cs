﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class VPersonCollectivesViewModel
    {
        public int? Id { get; set; }
        public int? IdPerson { get; set; }
        public int IdCollective { get; set; }
        public string Nombre { get; set; }
    }
}
