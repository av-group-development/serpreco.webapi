﻿using DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class StudioStateViewModel
    {
        public int Id { get; set; }
        public StudioStateEnum State { get; set; }
        public int? CancelOption { get; set; }
    }
}
