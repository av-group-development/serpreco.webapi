﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class VStudioViewModel
    {
        public int Id { get; set; }
        public int RamoId { get; set; }
        public string RamoTranslation { get; set; }
        public string RamoLanguage { get; set; }
        public DateTime EffectDate { get; set; }
        public int ProcessorId { get; set; }
        public string ProcessorUsername { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdate { get; set; }
        public int StateId { get; set; }
        public string StudioStateTranslation { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientSurname1 { get; set; }
        public string ClientSurname2 { get; set; }
        public string FullName { get; set; }
    }
}
