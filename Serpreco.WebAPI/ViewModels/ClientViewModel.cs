﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class ClientViewModel
    {
        public int Id { get; set; }
        public string Sperson { get; set; }
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public int IdDocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string Notes { get; set; }
        public bool IsPreClient { get; set; }
        public int IdLegalForm { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string SecondPhone { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdate { get; set; }
        public int BussinesAdvisor { get; set; }
        public int IdChargeMode { get; set; }
        public int IdSex { get; set; }
        public int IdLanguage { get; set; }
        public int IdTreatment { get; set; }
        public string FullName { get; set; }
    }
}
