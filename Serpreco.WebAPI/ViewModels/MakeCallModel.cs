﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class MakeCallModel
    {
        public string Extension { get; set; }
        public string Number { get; set; }
        public string UserId { get; set; }
    }
}
