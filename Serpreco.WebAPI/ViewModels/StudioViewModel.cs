﻿using DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class StudioViewModel
    {
        public int Id { get; set; }
        public int RamoId { get; set; }
        public string Notes { get; set; }
        public string IBAN { get; set; }
        public DateTime EffectDate { get; set; }
        public int ReasonId { get; set; }
        public int ProcessorId { get; set; }
        public CommercialAreaEnum CommercialAreaId { get; set; }
        public int SubAgentId { get; set; }
        public int PrescriberId { get; set; }
        public StudioStateEnum StateId { get; set; }
        public DateTime DateCreate { get; set; }
    }
}
