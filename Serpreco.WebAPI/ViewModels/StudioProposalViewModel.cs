﻿using DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class StudioProposalViewModel
    {
        public int StudioId { get; set; }
        public int ProposalId { get; set; }
        public int Id { get; set; }
        public int Company { get; set; }
        public string Notes { get; set; }
        public StudioProposalStateEnum StateId { get; set; }
        public double? TotalBonus { get; set; }
        public PeriodicityEnum? PeriodicityId { get; set; }
        public string Reason { get; set; }
        public string Proposal { get; set; }
    }
}
