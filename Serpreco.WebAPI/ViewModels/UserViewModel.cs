﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class UserViewModel
    {
        public bool authenticated { get; set; }
        public string name { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string userFirstSurname { get; set; }
        public string userSecondSurname { get; set; }
        public string userEmail { get; set; }
        public string userLang { get; set; }
        public string token { get; set; }
        public int processorCode { get; set; }
        public string signature { get; set; }
        public List<int> userRoleId { get; set; }
        public string newPassword { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string Extension { get; set; }
        public int DemarcacionId { get; set; }
    }
}
