﻿using Commons.Enums;
using System;


namespace API.ViewModels
{
    public class ReturnViewModel
    {
        public Object Result { get; set; } = null;
        public bool? ShowMessage { get; set; } = false;
        public int? Code { get; set; } = null;

        public ReturnViewModel(object result)
        {
            Result = result;
        }

        public ReturnViewModel(object _Result, bool _ShowMessage, ReturnVMEnum _Code)
        {
            Result = _Result;
            ShowMessage = _ShowMessage;
            Code = _Code.GetHashCode();
        }
    }
}
