﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class UserModel
    {
        public AspNetUsers UserInfo { get; set; }
        public List<int> Roles { get; set; }
    }
}
